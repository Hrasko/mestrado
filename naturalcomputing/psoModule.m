function [ best,bestFit,media,bestCorrectness,interactions,BestCounter ] = psoModule( n,steps,d,c1,c2,w,costFunction,samples,nC,classifications)



% restriction -> matrix with limits
%p -> pause between each interaction

%% Initialization
clc;
media = 0;

[ pos,localBestPos,gBest,gBestPos,fitness,localFitness,velocity ] = psoInit( n,steps,d,c1,c2,w,costFunction,samples,nC );

%% Drawing initialization
%xv = [-interval:0.1:interval];
%yv = [-interval:0.1:interval];
%[u,v] = meshgrid(xv,yv);
%s = funcao(u,v);
%nn = 1:n;
%gBestTracker = 0*ones(n);

%% Main Loop
interactions = 0;
bestCorrectness = Inf;
BestCounter = 0;
best = Inf;
bestFit = Inf;
overall = 0;
bestGuess = 1:3;
while (gBest > 0.001 && interactions < steps)    
    interactions = interactions + 1;
    
    [ pos,localBestPos,gBest,gBestPos,gIndex,fitness,localFitness,velocity ] = psoMainLoop( n,d,c1,c2,w, pos,localBestPos,gBest,gBestPos, fitness,localFitness, velocity, costFunction, samples, nC );
    
    %% Drawing
    K = size(samples,1);
    P = size(samples,2);
    
    currentBest = gBestPos(:,gIndex);
    guess = zeros(K,1);
    for i = 1:K
        x = samples(i,:);
        minD = Inf;
        for k = 1:nC
            s = 1+(k-1)*P;
            e = s+P-1;
            c = currentBest(s:e)';
            distance = abs(sum((x - c) .^2));
            if (distance < minD)
                guess(i) = k;
                minD = distance;
            end
        end
    end
    
    countG = zeros(nC,1);
    for i = 1:K
        countG(guess(i)) = countG(guess(i)) + 1;
    end
    
    countG = sort(countG);
    difference = abs(classifications-countG);
    minDiff = min(difference);
    maxDiff = max(difference);
    correctness = (sum(difference)/K) + (maxDiff - minDiff/K);
    currentFit = fitness(gIndex);
    
    if correctness < bestCorrectness
        bestCorrectness = correctness;
        best = currentBest;
        bestFit = currentFit;
        bestGuess = countG;
        BestCounter = interactions;
    end
    
    distance = correctness * K;
    overall = overall + distance;
    media = overall / interactions;
    
    clc;
    fprintf('iteracao: %d best encontrado em: %d media: %f  \n',interactions,BestCounter, media);
    fprintf('Current : \n fitness: %f correctness: %f guess: %d,%d,%d \n',currentFit,correctness,countG(1),countG(2),countG(3));
    fprintf('Best : \n fitness: %f correctness: %f guess: %d,%d,%d \n',bestFit,bestCorrectness,bestGuess(1),bestGuess(2),bestGuess(3));
    
    
    
end 