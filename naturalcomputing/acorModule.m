function [ best,fit,media,bestCorrectness,counter,BestCounter ] = acorModule( f, samples,classifications, nC,K,D,q,theta1,theta2,maxInteractions, restriction )
%ACORMODULE Summary of this function goes here
%   Detailed explanation goes here

%TODO: colocar os samples completos com a classificacao e refazer

[ phero,fitness,index ] = acorInit( f,samples,D, K,nC );

%main loop
counter = 0;
correctness = 1;
countG = zeros(nC,1);
overall = 0;
bestCorrectness = Inf;
w = acorWeight(q,K);
P = D / nC;
while(correctness > 0.01 && counter <= maxInteractions)
    counter = counter +1;   
    
     [ phero,fitness,index,sorted ] = acorMainLoop( f,samples,nC,phero,fitness,index, w, D,K,theta1,theta2 );
    
    
    currentBest = phero(:,index(1));
    guess = zeros(K,1);
    for i = 1:K
        x = samples(i,:);
        minD = Inf;
        for k = 1:nC
            s = 1+(k-1)*P;
            e = s+P-1;
            c = currentBest(s:e)';
            d = abs(sum((x - c) .^2));
            if (d < minD)
                guess(i) = k;
                minD = d;
            end
        end
    end
    
    countG = zeros(nC,1);
    for i = 1:K
        countG(guess(i)) = countG(guess(i)) + 1;
    end
    
    %countG = sort(countG);
    difference = abs(classifications-countG);
    minDiff = min(difference);
    maxDiff = max(difference);
    correctness = (sum(difference)/K) + (maxDiff - minDiff/K);
    clc;    
    if correctness < bestCorrectness
        bestCorrectness = correctness;
        best = currentBest;
        fit = sorted(1);
        bestGuess = countG;
        BestCounter = counter;
    end
    
    
    
    distance = correctness * K;
    overall = overall + distance;
    media = overall / counter;
    
    fprintf('iteracao: %d best encontrado em: %d \n',counter,BestCounter);
    fprintf('Current : \n fitness: %f  guess: %d,%d,%d \n',sorted(1),countG(1),countG(2),countG(3));
    fprintf('Best : \n fitness: %f  guess: %d,%d,%d \n',fit,bestGuess(1),bestGuess(2),bestGuess(3));
    fprintf('media: %f correctness: %f',media, correctness);
end



end

