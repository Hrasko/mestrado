function [ w ] = acorWeight( q, K )
%ACORWEIGHT Summary of this function goes here
%   Detailed explanation goes here

w = zeros(K,1);
for i = 1:K
	w(i) = exp( -1*((i-1)^2) / (2 * q^2 * K^2)) / (q * K * sqrt(2*pi));
end

end

