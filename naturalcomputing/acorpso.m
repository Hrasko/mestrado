function [ best,BestFit,media,bestCorrectness,counter,BestCounter ] = acorpso( f, samples,classifications, nC,K,D,q,theta1,theta2,maxInteractions, n,steps,d,c1,c2,momentum)

%% Initialization
clc;

[ phero,antfitness,antindex ] = acorInit( f,samples,D, K,nC );

[ pos,localBestPos,gBest,gBestPos,psofitness,localFitness,velocity ] = psoInit( n,steps,d,c1,c2,momentum,f,samples,nC );

%main loop
counter = 0;
correctness = 1;
countG = zeros(nC,1);
overall = 0;
bestCorrectness = Inf;
bestFit = Inf;

w = acorWeight(q,K);
P = D / nC;
whoiswinning = 0;
while(correctness > 0.01 && counter <= maxInteractions)
    counter = counter +1;
    
    [ phero,antfitness,antindex,sorted ] = acorMainLoop( f,samples,nC,phero,antfitness,antindex, w, D,K,theta1,theta2 );
    [ pos,localBestPos,gBest,gBestPos,gIndex,psofitness,localFitness,velocity ] = psoMainLoop( n, d,c1,c2,momentum,pos,localBestPos,gBest,gBestPos, psofitness,localFitness, velocity, f, samples, nC );
    
    %%global exchange
    currentBest = phero(:,antindex(1));
    %if 1 >2 % gBest < sorted(1)
    %    if gBest < bestFit
    %        bestFit = gBest;
    %        currentBest = gBestPos(:,1);
    %        if mod(counter,K) == 0
    %            phero(:,antindex(1)) = currentBest;    
    %            antfitness(antindex(1)) = bestFit;
    %        end
    %    end
    %else
    %    if sorted(1) < bestFit
    %        bestFit = sorted(1);
    %        currentBest = phero(:,antindex(1));
    %        pos(:,gIndex) = currentBest;
    %        psofitness(gIndex) = bestFit;
    %        gBest = bestFit;
    %    end
    %end
    
    
    %% Drawing
    
    [ antCorrectness,antGuess ] = clusterClassificationFitness( samples,classifications,phero(:,antindex(1)),K,P,nC );
    [ psoCorrectness,psoGuess ] = clusterClassificationFitness( samples,classifications,gBestPos(:,1),K,P,nC );
    
    if (antCorrectness < psoCorrectness)
        correctness = antCorrectness;
        countG = antGuess;
        currentfit = sorted(1);
        currentBest = phero(:,antindex(1));
        pos(:,gIndex) = currentBest;
        psofitness(gIndex) = bestFit;
        whoiswinning = 1;
    else
        correctness = psoCorrectness;
        countG = psoGuess;
        currentfit = gBest;
        currentBest = gBestPos(:,1);
        whoiswinning = 2;
    end
    
    if correctness < bestCorrectness
        bestCorrectness = correctness;
        best = currentBest;
        BestFit = currentfit;
        bestGuess = countG;
        BestCounter = counter;
        if (psoCorrectness < antCorrectness)
            phero(:,antindex(1)) = currentBest;    
            antfitness(antindex(1)) = BestFit;
        end
    end
    
    distance = correctness * K;
    overall = overall + distance;
    media = overall / counter;
    
    clc;
    fprintf('iteracao: %d best encontrado em: %d \n',counter,BestCounter);
    fprintf('ACOR : \n BestOfAnt: %f guess: %d,%d,%d \n',sorted(1),antGuess(1),antGuess(2),antGuess(3));
    fprintf('PSO : \n gBest: %f guess: %d,%d,%d \n',gBest,psoGuess(1),psoGuess(2),psoGuess(3));
    fprintf('Best : \n fitness: %f  guess: %d,%d,%d \n',BestFit,bestGuess(1),bestGuess(2),bestGuess(3));
    fprintf('media: %f correctness: %f melhor por: %d',media, correctness, whoiswinning);
    
end 