function [ ] = drawEx1( p,nn,localFitness,gBest,gBestPos,gBestTracker,varargin )
%DRAWPSO Summary of this function goes here
%   Detailed explanation goes here

    if (p > 0)

        subplot(3,1,1);
        plot (nn,localFitness,'x',nn,gBest,'ro');
        title('Fitness');
        grid on

        subplot(3,1,2);
        plot(gBestTracker);
        title('gBest');
        
        v = varargin{1};
        xfile = v{1};
        yfile = v{2};
        K = gBestPos(1);
        G = gBestPos(2);
        D = gBestPos(3);        
        
        t = size(xfile);
        parcial = 1:t;
        for i = 1:t
            parcial(i) = K./(1+exp(-G.*(xfile(i)-D)));
        end        
        
        subplot(3,1,3);
        plot(xfile,parcial,'*',xfile,yfile,'o');        
        
        pause(p)
    end
end

