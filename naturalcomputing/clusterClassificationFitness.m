function [ fitness,countG ] = clusterClassificationFitness( samples,classifications,currentBest,K,P,nC )
%CLUSTERCLASSIFICATIONFITNESS Summary of this function goes here
%   Detailed explanation goes here

guess = zeros(K,1);
for i = 1:K
    x = samples(i,:);
    minD = Inf;
    for k = 1:nC
        s = 1+(k-1)*P;
        e = s+P-1;
        c = currentBest(s:e)';
        distancia = abs(sum((x - c) .^2));
        if (distancia < minD)
            guess(i) = k;
            minD = distancia;
        end
    end
end

countG = zeros(nC,1);
for i = 1:K
    countG(guess(i)) = countG(guess(i)) + 1;
end

%countG = sort(countG);
difference = abs(classifications-countG);
minDiff = min(difference);
maxDiff = max(difference);
fitness = (sum(difference)/K) + (maxDiff - minDiff/K);

end


