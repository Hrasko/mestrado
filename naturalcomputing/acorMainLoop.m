function [ phero,fitness,index,sorted ] = acorMainLoop( f,samples,nC,phero,fitness,index, w, D,K,theta1,theta2 )
%ACORMAINLOOP Summary of this function goes here
%   Detailed explanation goes here

    Kminus = K -1;    

    newAnts = ones(D,K);
    
    for ant = 1:K
        for parameter = 1:D
            dev = 1;
            avg = 0;            
            
            if rand<theta2                
                avgIndex = index(rouletteWheel(w));
                avg = phero(parameter,avgIndex);
                if rand < theta1
                    for antDevIndex = 1:K
                        dev = dev + (phero(parameter,antDevIndex) - avg);
                    end
                    dev = dev /Kminus;
                else
                    dev = normrnd(0,1);
                end

                newAnts(parameter,ant) = abs(normrnd(avg,abs(dev)));
            else
                newAnts(parameter,ant) = rand;
            end
            
            
            if newAnts(parameter,ant) > 1
                newAnts(parameter,ant) = 1;
            end
        end
    end

    phero = newAnts;
    for m=1:K
        fitness(m) = f(samples,phero(:,m),nC);
    end
    [sorted,index] = sort(fitness);

end

