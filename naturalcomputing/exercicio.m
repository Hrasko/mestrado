function [y]=exercicio(K,G,D,xfile,yfile,p)    
    total = 0;
    t = length(xfile);
    parcial = 1:t;
    for i = 1:t
        parcial(i) = K./(1+exp(-G.*(xfile(i)-D)));
        erro = (yfile(i)-parcial(i)).^2;
        total= total + erro;
    end
    y = total/t;
    if (p>0)
        subplot(3,2,2);
        plot(xfile,parcial,'*',xfile,yfile,'o');
    end
end