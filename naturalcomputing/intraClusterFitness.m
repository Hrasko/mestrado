function [ fitness ] = intraClusterFitness( samples, clusters, nC)
%INTRACLUSTERFITNESS Summary of this function goes here
%   Detailed explanation goes here 

nP = size(samples,2);

clusterCenters = clusters;%ones(nC,nP);

%k = 1;
%for i = 1:nP
%    for j = 1:nC
%       clusterCenters(j,i) = (clusters(k) - restrictions(i,1)) ./ (restrictions(i,2) - restrictions(i,1));
%       k = k+1;
%    end
%end

s = 0;
for i = 1:nP
    x = samples(i,:);
    minD = Inf;
    for k = 1:nC
        si = 1+(k-1)*nP;
        e = si+nP-1;
        c = clusterCenters(si:e)';
        d = abs(sum((x - c) .^2));
        if (d < minD)
            minD = d;
        end
    end
    s = s + d;
end

d = 0;
for i = 1:nC
    si = 1+(i-1)*nP;
    ei = si+nP-1;
    ci = clusterCenters(si:ei)';
    for j = i+1:nC
        
        sj = 1+(j-1)*nP;
        ej = sj+nP-1;
        cj = clusterCenters(sj:ej)';
        
        d = d + abs(sum(ci - cj));
    end
end

fitness = s / d;

end

