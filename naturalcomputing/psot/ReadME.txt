PSOt, particle swarm optimization toolbox for matlab. 

May be distributed freely as long as none of the files are modified. 

Send suggestions to bkbirge@unity.ncsu.edu. 

Updates will be posted periodically at www4.ncsu.edu/~bkbirge

To install:
Just run the executable, it will self extract all the files into a directory called 'PSOt'. Then just modify your Matlab path to take advantage of the new directory and you'll be up an running!