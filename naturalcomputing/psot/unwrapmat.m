function [O,varargout]=unwrapmat(varargin)
% UNWRAPMAT
%  converts any amount of matrices into a single row vector
%
%  usage:
%     [O]=UNWRAPMAT(M1,M2,...,MD)
%  or optionally:
%     [O,R,C]=UNWRAPMAT(M1,M2,...,MD)
%
%  output:
%   O = row vector output
%   R = row vector of row sizes for each input matrix
%   C = row vector of column sizes for each input matrix
%
%  input:
%   M1,M2,....,MD  = various sized matrices that you want to vectorize
% 
%  note: R,C are needed to wrap the original matrices back up, so 
%  don't lose 'em!
%
%  See also:  WRAPMAT

% Brian Birge
% Rev 1.0
% 7/20/01

if nargin<1
   error('Not enough input arguments.'); 
end

for i=1:nargin
   temp=(varargin{i});
   [r(i),c(i)]=size(temp);
   eval(['part',num2str(i),'=reshape(temp,1,r(i)*c(i));']);
   if i==1
      o=part1;
   else      
      eval(['o=[o,part',num2str(i),'];']);
   end   
end

if nargout==3
   varargout{1}=r;
   varargout{2}=c;
end

O=o;