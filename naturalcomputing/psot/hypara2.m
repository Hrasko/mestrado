function [y]=hypara2(x1,x2,x3,x4,x5,x6,x7)
% hypara2.m
% function to generate an 8D hyberbolic paraboloid
% 
% y=x1^2 + x2^2 + x3^2 + x4.^2 + x5.^2 + x6.^2 + x7.^2

y=x1.^2 + x2.^2 + x3.^2 + x4.^2 + x5.^2 + x6.^2 + x7.^2;