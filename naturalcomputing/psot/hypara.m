function [y]=hypara(x1,x2,x3)
% hypara.m
% function to generate a 4D hyberbolic paraboloid
% 
% y=x1^2 + x2^2 + x3^2

y=x1.^2 + x2.^2 + x3.^2;