function [y]=exercicio(K,G,D)    
    total = 0;
    xfile = dlmread('X_File.txt');
    yfile = dlmread('Y_File.txt');
    t = length(xfile);
    parcial = 1:t;
    for i = 1:t
        parcial(i) = K./(1+exp(-G.*(xfile(i)-D)));
        erro = (yfile(i)-parcial(i)).^2;
        total= total + erro;
    end
    y = total;
    subplot(3,1,3);
    plot(xfile,parcial,'*',xfile,yfile,'o');
end