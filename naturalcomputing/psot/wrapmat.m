function [varargout]=wrapmat(in,r,c)
% WRAPMAT
%  converts a single row vector to one or more
%  RxC matrices
%
%  usage:
%     [M1,M2,...,MD]=WRAPMAT(O,R,C)
%
%  input:
%   O = row vector to matricize
%   R = row vector of row sizes for each input matrix
%   C = row vector of column sizes for each input matrix
%
% note: the sum of rows*columns must be the same
%       as the total length of the vector O
%    or in other words: the length of the R and C vectors
%       must be equal to each other and the number of output
%       matrices you want
%
%  output:
%   M1,M2,....,MD  = various sized matrices formed
% 
%  See also:  UNWRAPMAT

% Brian Birge
% Rev 1.0
% 7/25/01

if nargin<3
   error('Not enough input arguments.'); 
end

for k=1:nargout
   varargout{k}=reshape(in(1:r(k)*c(k)),r(k),c(k));
   in=in(r(k)*c(k)+1:end);
end
