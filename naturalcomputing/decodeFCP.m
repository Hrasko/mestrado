function [ fuzzyDecoded ] = decodeFCP( pos, fuzzyBefore )
%DECODEFCP Summary of this function goes here
%   Detailed explanation goes here
fuzzyDecoded = fuzzyBefore;

fuzzyDecoded.input(1).mf(1).params = sort([-10000 -pos(1) -pos(2)]);
fuzzyDecoded.input(1).mf(2).params = sort([-pos(3) -pos(4) -pos(5)]);
fuzzyDecoded.input(1).mf(3).params = sort([-pos(6) -pos(7) -pos(8)]);
fuzzyDecoded.input(1).mf(4).params = [-pos(9) 0 pos(9)];
fuzzyDecoded.input(1).mf(5).params = sort([pos(8) pos(7) pos(6)]);
fuzzyDecoded.input(1).mf(6).params = sort([pos(5) pos(4) pos(3)]);
fuzzyDecoded.input(1).mf(7).params = sort([pos(2) pos(1) 10000]);

fuzzyDecoded.input(2).mf(1).params = sort([-10000 -pos(10) -pos(11)]);
fuzzyDecoded.input(2).mf(2).params = sort([-pos(12) -pos(13) -pos(14)]);
fuzzyDecoded.input(2).mf(4).params = [-pos(15) 0 pos(15)];
fuzzyDecoded.input(2).mf(5).params = sort([pos(14) pos(13) pos(12)]);
fuzzyDecoded.input(2).mf(3).params = sort([pos(11) pos(10) 10000]);

fuzzyDecoded.output(1).mf(7).params = sort([-10000 -pos(16) -pos(17)]);
fuzzyDecoded.output(1).mf(1).params = sort([-pos(18) -pos(19) -pos(20)]);
fuzzyDecoded.output(1).mf(2).params = sort([-pos(21) -pos(22) -pos(23)]);
fuzzyDecoded.output(1).mf(3).params = [-pos(24) 0 pos(24)];
fuzzyDecoded.output(1).mf(4).params = sort([pos(23) pos(22) pos(21)]);
fuzzyDecoded.output(1).mf(5).params = sort([pos(20) pos(19) pos(18)]);
fuzzyDecoded.output(1).mf(6).params = sort([pos(17) pos(16) 10000]);

end

