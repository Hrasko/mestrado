clear ; close all; clc;

samplesRaw = dlmread('heart.data');
% number of samples
nS = size(samplesRaw,1);

%number of parameters in each sample
nP = size(samplesRaw,2)-1;

%number of Clusters
nC = 5;

restrictions = zeros(nP*nC,2);

i = 1;
for c = 1:nC
	for p = 1:nP
		restrictions(i,1) =  min(samplesRaw(:,p));
		restrictions(i,2) =  max(samplesRaw(:,p));
		i = i+1;
	end
end

m1 = ones(nS,nP);
m2 = m1;

for i = 1:nP
    m1(:,i) = restrictions(i,1);
    m2(:,i) = restrictions(i,2);
end

samples = (samplesRaw(:,1:nP) - m1)./(m2 - m1);
classifications = samplesRaw(:,(nP+1));
countT = zeros(nC,1);
for i = 1:nS
    countT(classifications(i)+1) = countT(classifications(i)+1) + 1;
end
sortedT = sort(countT);

execucoes = 20;
resultados = zeros(execucoes,5);

maxIterections = nS*10;

for execucao = 1:execucoes
    %[ best,bestFit,media,bestCorrectness,counter,BestCounter ] = acorModule( @intraClusterFitness, samples,sortedT,nC,nS,nP*nC,0.85,0.7,0.7,maxIterections, restrictions );
    %[ best,bestFit,media,bestCorrectness,counter,BestCounter ] = psoModule(50,maxIterections,nC*nP,1.49445, 1.49445, 0.729,@intraClusterFitness,samples,nC,sortedT);
    [ best,bestFit,media,bestCorrectness,counter,BestCounter ] = acorpso( @intraClusterFitness, samples,sortedT, nC,nS,nP*nC,0.85,0.7,0.7,maxIterections, 50,maxIterections,nC*nP,1.49445, 1.49445, 0.729);
    resultados(execucao,1) = bestFit;
    resultados(execucao,2) = bestCorrectness;
    resultados(execucao,3) = counter;    
    resultados(execucao,4) = BestCounter;
    resultados(execucao,5) = media;
    dlmwrite('heart-hibModule.txt',resultados);
end
