function [ pos,localBestPos,gBest,gBestPos,fitness,localFitness,velocity ] = psoInit( n,steps,d,c1,c2,momentum,costFunction,samples,nC )
%PSOINIT Summary of this function goes here
%   Detailed explanation goes here

%fitness indicates how well is the particule doing.
fitness = ones(n,1);


% randomize positions
pos = rand(d,n);
localBestPos = pos;

% randomize initial velocity. 
velocity = momentum*rand(d,n);

% evaluate initial positions
for i= 1: n

   fitness(i) = costFunction(samples,pos(:,i),nC);
   
end

% Initialy, local fitness is current fitness
localFitness = fitness;

% Find minimum fitness (gBest) on gIndex
[gBest,gIndex] = min(localFitness) ;

% Get Position of gBest. IT is a vector to simplify the velocity
% calculation in the main loop
gBestPos = pos;
for i= 1: n
    gBestPos(:,i) = pos(:,gIndex);
end

% Walk the particles
for i=1:n
    for j=1:d
        target = pos(j,i) + velocity(j,i);
        if (target >= 0 && target <= 1)
            pos(j,i) = target;
        end
    end
end

end

