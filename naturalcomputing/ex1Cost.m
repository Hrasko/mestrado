function [y]=ex1Cost(pos, varargin )    
    v = varargin{1};
    K = pos(1);
    G = pos(2);
    D = pos(3);
    
    xfile = v{1};
    yfile = v{2};
    
    total = 0;
    t = length(xfile);
    parcial = 1:t;
    for i = 1:t
        parcial(i) = K./(1+exp(-G.*(xfile(i)-D)));
        erro = (yfile(i)-parcial(i)).^2;
        total= total + erro;
    end
       
    y = total/t;   
end