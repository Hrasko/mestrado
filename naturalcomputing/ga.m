function [ best ] = ga( n,maxGen,p,restriction,cost,selection,sParams,crossover,cParams,mutation,mParams)

% n-> number of individuals
% maxGen -> maximum number of generations
% p -> pause time between each drawing
% restriction -> matrix with limits
% cost,selection,crossover,mutation -> functions
% sParams,cParams,mParams -> vector with params ofits respective function

%% Initialization
clc;

%This work is tested only with three parameters.
d = 3;


% randomize population between -interval and interval. First dimension is used for fitness
pop = ones(d+1,n);
for i=1:n
    for j=1:d
        pop(j+1,i) = rand(1)*(restriction(j,2)-restriction(j,1))+restriction(j,1);
    end
end

%other init
xfile = dlmread('X_File.txt');
yfile = dlmread('Y_File.txt');


%% Drawing initialization
%xv = [-interval:0.1:interval];
%yv = [-interval:0.1:interval];
%[u,v] = meshgrid(xv,yv);
%s = funcao(u,v);
if (p>0)
    nn = 1:n;
    fBestTracker = 0*ones(n);
    set(gcf,'Position',[66  1 1301 689]);
end
%% Main Loop

generation = 0;
while (generation < maxGen)    
    generation = generation + 1;
    
    % evaluate population
	for i= 1: n
	   pop(1,i) = cost(pop(2,i),pop(3,i),pop(4,i),xfile,yfile,p);
	end
	fBest = min(pop(1,:));    
    
    % Do the Selection 
    selectedPop = selection(pop,fitness,sParams);
    
    % Do the crossover
	pop = crossover(selectedPop,pop,fitness,cParams);

    % Mutate
	pop = mutation(pop,mParams);
    
    %% Drawing
    if (p > 0)
        x=pos(1,:);
        y=pos(2,:);
        z=pos(3,:);
        %subplot(2,2,1)
        %surfc(u,v,s,'FaceColor','interp',...
       %'EdgeColor','none',...
       %'FaceLighting','phong');
        %hold on    
        %scatter3(x,y,funcao(x,y));    
        %hold off

        subplot(4,2,1);
        plot (nn,x,'x');
        title('Valores de K');
        axis([1 n restriction(1,1) restriction(1,2)]);
        grid on

        subplot(4,2,3);
        plot (nn,y,'x');
        title('Valores de G');
        axis([1 n restriction(2,1) restriction(2,2)]);
        grid on

        subplot(4,2,5);
        plot (nn,z,'x');
        title('Valores de D');
        axis([1 n restriction(3,1) restriction(3,2)]);
        grid on

        subplot(4,2,7);
        plot (nn,localFitness,'x',nn,gBest,'ro');
        title('Fitness');
        grid on

        subplot(3,2,4);
        scatter3(x,y,z,'h');
        axis([restriction(1,1) restriction(1,2) restriction(2,1) restriction(2,2) restriction(3,1) restriction(3,2) ]);
        title('Position');

        subplot(3,2,6);
        plot(gBestTracker);
        title('gBest');

        cost(gBestPos(1,1),gBestPos(2,1),gBestPos(3,1),xfile,yfile,p);

        %plot(nn,interactions);    
        %axis([0 n 0 n]);    

        pause(p)
    end
end 
