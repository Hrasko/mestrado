clear ; close all; clc

fileNames = ['bcspwr01.mtx'; 'bcsstk19.mtx'; 'can_61__.mtx'; 'dwt_2680.mtx'; 'netscien.mtx'; 'teste1__.mtx'; 'bcspwr04.mtx'; 'bfwb62__.mtx'; 'can_73__.mtx'; 'dwt_492_.mtx'; 'power___.mtx'; 'bcspwr07.mtx'; 'bfwb782_.mtx'; 'can_838_.mtx'; 'eris1176.mtx'; 'qc324___.mtx'; 'bcspwr09.mtx'; 'can_268_.mtx'; 'dwt_209_.mtx'; 'jagmesh3.mtx'; 'rail1357.mtx'];
fSize = size(fileNames,1);

resultados=zeros(fSize*10,6);

t = 0;
for name=1:fSize
    A = mmread (fileNames(name,:));
    [i,j] = find(A);
    lb_inicial = max(i-j);
    mSize = size(A,1);
    tic;
    p = symrcm(A);
    rcmTime = toc;
    I = eye(mSize,mSize);
    P = I(p,:);
    RCM = P*A*P';

    [i,j] = find(RCM);
    rcmLB = max(i-j);
    
    restrictions = zeros(mSize,2);
    for i=1:mSize
        restrictions(i,2) = 1;
    end

    for execucoes=1:2
        t = t+1;
        tic;
        [gBest,v] = pso([30 100 mSize 1.49445 1.49445 0.729 name execucoes],restrictions,@larguraBandaCost,0,@drawT1,A,mSize,rcmLB);
        psoTime = toc;
        [lixo,indices] = sort(v);
        I = eye(mSize,mSize);
        P = I(indices,:);
        PSO = P*A*P';
        
        subplot(1,2,1),spy(RCM),title('RCM');
        subplot(1,2,2),spy(PSO),title('PSO');
        
        resultados(t,1) = name;
        resultados(t,2) = execucoes;
        resultados(t,3) = rcmLB;
        resultados(t,4) = rcmTime;
        resultados(t,5) = gBest;
        resultados(t,6) = psoTime;
        
        print(num2str(t),'-djpeg');
    end
end

xlswrite('resultados.xls',resultados);