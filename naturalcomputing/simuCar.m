function [ updatedPos ] = simuCar( previousPos, u, v )
%SIMUCAR Summary of this function goes here
%   Detailed explanation goes here
    updatedPos = 1:3;
    
    theta = degtorad(previousPos(1));
    
    updatedPos(1) = previousPos(1) + v*tan(degtorad(u));    
    updatedPos(2) = previousPos(2) + v*cos(theta);
    updatedPos(3) = previousPos(3) + v*sin(theta);
end

