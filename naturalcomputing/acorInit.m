function [ phero,fitness,index ] = acorInit( f,samples,D,K,nC )
%ACORINIT Summary of this function goes here
%   Detailed explanation goes here


phero = ones(D,K);
fitness = ones(1,K);


% Random Init
for m=1:K
    for d=1:D
        phero(d,m) = rand;
    end
    fitness(m) = f(samples,phero(:,m),nC);
end
[lixo,index] = sort(fitness);


end

