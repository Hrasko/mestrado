x = 0;
y = 100;
theta = 0;
pos = [theta x y];

time = 5000;
plotX = 1:time;
plotY = 1:time;
plotTheta = 1:time;
generations = 1;

myFuzzy = readfis('fcpex2.fis');

for i = 1:time
        plotTheta(i) = pos(1);
        plotX(i) = pos(2);
        plotY(i) = pos(3);

        u = evalfis([pos(3) pos(1)], myFuzzy);

        pos = simuCar(pos,u,5);
        if abs(pos(3)) < 0.01 && abs(pos(1)) < 0.1
            break;
        end
end

display(i);

plotT = 1:time;
subplot(2,3,1);
plot (plotT,plotX);
title('X');

subplot(2,3,2);
plot (plotT,plotY);
title('Y');

subplot(2,3,3);
plot(plotT,plotTheta);
title('theta');

subplot(2,1,2);
plot(plotX,plotY);
title('position');


    
