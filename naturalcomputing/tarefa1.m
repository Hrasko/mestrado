clear ; close all; clc

%

%plot(1:size(yt),yt,'O');

% teste
teste = dlmread('teste1.txt');
t = dlmread('treinamento1.txt');
tSize = length(t(:,1));

rp = randperm(tSize);
training = zeros(tSize,2);
for i = 1:tSize
    training(i,1) = t(rp(i),1);
    training(i,2) = t(rp(i),2);
end

nnConfig = [1 20 1];

vSize = 0;
for i=2:length(nnConfig)
    prev = nnConfig(i-1);
    next = nnConfig(i);
    vSize = vSize + ((prev+1) * next);
end    

restrictions = zeros(vSize,2);
searchSpaceMaxValue = 15;
for i=1:vSize
    restrictions(i,1) = -searchSpaceMaxValue;
    restrictions(i,2) = searchSpaceMaxValue;
end
[gBest,v] = pso([50 10 vSize 1.49445 1.49445 0.729],restrictions,@t1cost,0,@drawT1,training,tSize,nnConfig,teste);

display(gBest);

theta = decodeTheta(v, nnConfig);
display(theta);
%theta1 = theta(1:2,:,1);
%theta2 = theta(:,1:1,2);

%theta1 = rand(nInput,neurons);
%theta2 = rand(neurons+1,nOutput);

%yp = 1:tSize;
%for i=1:tSize
%    yp(i) = nn (training(i,1),theta1,theta2);
%end




yt = 1:length(teste);
for i=1:length(teste)
   yt(i) = nn (teste(i),theta, nnConfig);
end

r = 0:0.1:7;

yp = 1:length(r);
for i=1:length(r)
   yp(i) = nn (r(i),theta, nnConfig);
end

%plot
subplot(3,1,3);



plot(r,sin(r),'y-',training(:,1),training(:,2),'.',teste,yt,'r+',r,yp,'r-');
legend('funcao','training','test','prediction');
