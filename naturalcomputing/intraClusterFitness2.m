function [ fitness ] = intraClusterFitness2( clusters, samples,nC, classifications)
%INTRACLUSTERFITNESS Summary of this function goes here
%   Detailed explanation goes here

P = size(samples,2);
K = P*nC;
best = clusters;%zeros(K,1);
%k = 1;
%for i = 1:P
%    for j = 1:nC
%       best(k) = (clusters(k) - restrictions(i,1)) ./ (restrictions(i,2) - restrictions(i,1));
%       k = k+1;
%    end
%end


    guess = zeros(K,1);
    for i = 1:K
        x = samples(i,:);
        minD = Inf;
        for k = 1:nC
            s = 1+(k-1)*P;
            e = s+P-1;
            c = best(s:e)';
            d = abs(sum((x - c) .^2));
            if (d < minD)
                guess(i) = k;
                minD = d;
            end
        end
    end
    
    countG = zeros(nC,1);
    for i = 1:K
        countG(guess(i)) = countG(guess(i)) + 1;
    end

    
    %countG = sort(countG);

    difference = classifications-countG;
    minDiff = min(difference);
    maxDiff = max(difference);
    fitness = (sum(abs(difference))/K) + (abs(maxDiff - minDiff)/K);

end

