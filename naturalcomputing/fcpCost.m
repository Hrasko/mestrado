function [ cost ] = fcpCost( pos, varargin)
%FCPCOST Summary of this function goes here
%   Detailed explanation goes here
v = varargin{1};

myFuzzy = v{1};
inputSize = size(myFuzzy.input,2);
c = 1;
for i=1:inputSize
    mfSize = size(myFuzzy.input(i).mf,2);
    for j=1:mfSize
        myFuzzy.input(i).mf(j).params = sort([pos(c) pos(c+1) pos(c+2)]);
        c = c+3;
    end
end

outputSize = size(myFuzzy.output,2);
for i=1:outputSize
    mfSize = size(myFuzzy.output(i).mf,2);
    for j=1:mfSize
        myFuzzy.output(i).mf(j).params = sort([pos(c) pos(c+1) pos(c+2)]);
        c = c+3;
    end
end

x = 0;
y = 100;
theta = 0;
p = [theta x y];

for i = 1:time
        
        u = evalfis([p(3) p(1)], myFuzzy);

        p = simuCar(p,u,5);
        if abs(p(3)) < 0.01 && abs(p(1)) < 0.1
            break;
        end
end

display(i);

end