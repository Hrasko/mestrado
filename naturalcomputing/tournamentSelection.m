function [ selectedPop ] = tournamentSelection(pop,fitness,sParams)
	%tournament size
	ts = sParams(1);
	%selection percentage
	sp = sParms(2);
	%fitness vector
	fitness = pop(1,:);
	%population size
	n = size(fitness);
	
	m = n * sp;

	tournament = [1:ts];

	selectedPop = [1:m];
	[best,selectedPop(1)] = min(fitness);
	for i=2:m		
		for j=1:ts
			t(j) = pop(rand(n));
		end
		[aux,selectedPop(i)] = min(t(1,:));
	end
end 
