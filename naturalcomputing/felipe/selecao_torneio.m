function out=selecao_torneio(Pk0,Pg0,Pd0,F0,inputX,inputY)
    Pk=Pk0;
    Pg=Pg0;
    Pd=Pd0;
    F=F0;
    n=length(Pk0);
    for i=1:n
        individuo1=natural_rand(1,n);
        individuo2=natural_rand(1,n);
        
        if F0(individuo1)<F0(individuo2)
            Pk(i)=Pk0(individuo1);
            Pg(i)=Pg0(individuo1);
            Pd(i)=Pd0(individuo1);            
        else
            Pk(i)=Pk0(individuo2);
            Pg(i)=Pg0(individuo2);
            Pd(i)=Pd0(individuo2);
        end
    end
    out=[Pk,Pg,Pd];
end