% GERADOR DE NÚMEROS NATURAIS ALEATORIOS
% LOWER = LIMITE INFERIOR DO NÚMERO
% UPPER = LIMITE SUPERIOR DO NÚMERO
function [out] = natural_rand(lower, upper)
    out = floor(rand()*upper)+lower;
end