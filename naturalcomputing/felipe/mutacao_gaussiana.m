% MUTACAO GAUSSIANA
% SUBSTITUI O GENE POR UM NUMERO ALEATORIO DE UMA DISTRIBUICAO GAUSSIANA
% P=POPULACAO
% I=INDIVIDUO A SER MUTADO
% OUT=MUTACAO DO INDIVIDUO
function [out]=mutacao_gaussiana(P, i)
    n=length(P);
    j=natural_rand(1,n);
    if i==j
        c=distribuicao_gaussiana(1,P(i)); % desvio=1, media=P(i)
    else
        c=P(i);
    end
    out=c;
end