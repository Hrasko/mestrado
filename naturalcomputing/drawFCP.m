function [ ] = drawFCP( exec,time,x,y,theta,myFuzzy )
%DRAWFCP Summary of this function goes here
%   Detailed explanation goes here

pos = [theta x y];

plotX = 1:time;
plotY = 1:time;
plotTheta = 1:time;


for i = 1:time
        plotTheta(i) = pos(1);
        plotX(i) = pos(2);
        plotY(i) = pos(3);

        u = evalfis([pos(3) pos(1)], myFuzzy);

        pos = simuCar(pos,u,5);
        if abs(pos(3)) < 0.01 && abs(pos(1)) < 1
            break;
        end
end
display(i);
%subplot(1,1,exec);
plot(plotX,plotY);
%title(exec);


end

