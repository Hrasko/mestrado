function [ best,bestFit,media,bestCorrectness,BestCounter ] = drawClustering( currentBest,currentFit,best,bestFit,bestGuess,counter,BestCounter,overall,bestCorrectness,varargin )
%DRAWCLUSTERING Summary of this function goes here
%   Detailed explanation goes here
    v = varargin{1};

    samples = v{1};
    nC = v{2}; 
    classifications = v{3};
    
    K = size(samples,1);
    P = size(samples,2);
    
    guess = zeros(K,1);
    for i = 1:K
        x = samples(i,:);
        minD = Inf;
        for k = 1:nC
            s = 1+(k-1)*P;
            e = s+P-1;
            c = currentBest(s:e)';
            d = abs(sum((x - c) .^2));
            if (d < minD)
                guess(i) = k;
                minD = d;
            end
        end
    end
    
    countG = zeros(nC,1);
    for i = 1:K
        countG(guess(i)) = countG(guess(i)) + 1;
    end
    
    %countG = sort(countG);
    difference = abs(classifications-countG);
    minDiff = min(difference);
    maxDiff = max(difference);
    correctness = (sum(difference)/K) + (maxDiff - minDiff/K);
    clc;
    if correctness < bestCorrectness
        bestCorrectness = correctness;
        best = currentBest;
        bestFit = currentFit;
        bestGuess = countG;
        BestCounter = counter;
    end
    
    
    
    distance = correctness * K;
    overall = overall + distance;
    media = overall / counter;
    
    fprintf('iteracao: %d best encontrado em: %d \n',counter,BestCounter);
    fprintf('Current : \n fitness: %f  guess: %d,%d,%d \n',currentFit,countG(1),countG(2),countG(3));
    fprintf('Best : \n fitness: %f  guess: %d,%d,%d \n',bestFit,bestGuess(1),bestGuess(2),bestGuess(3));
    fprintf('media: %f correctness: %f',media, correctness);

end

