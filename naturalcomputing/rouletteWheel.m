function [ i ] = rouletteWheel( w )
%ROULETTEWHEEL Summary of this function goes here
%   Detailed explanation goes here

sw = sum(w);
p = w./sw;
i = 1;
s = size(w,1);
r = rand(1,1);
while (p(i) < r & i < s)
	r = r - p(i);
	i = i+1;	
end
end