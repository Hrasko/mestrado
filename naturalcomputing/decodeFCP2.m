function [ fuzzyDecoded ] = decodeFCP2( pos, fuzzyBefore )
%DECODEFCP Summary of this function goes here
%   Detailed explanation goes here
fuzzyDecoded = fuzzyBefore;

inp1 = pos;
inp1(10:24) = [];
inp1 = sort(inp1);
inp2 = pos;
inp2(16:24) = [];
inp2(1:9) = [];
inp2 = sort(inp2);

out1 = pos;
out1(1:15) = [];
out1 = sort(out1);

fuzzyDecoded.input(1).mf(1).params = [-10000 -inp1(9) -inp1(5)];
fuzzyDecoded.input(1).mf(2).params = [-inp1(8) -inp1(6) -inp1(2)];
fuzzyDecoded.input(1).mf(3).params = [-inp1(7) -inp1(3) -inp1(1)];
fuzzyDecoded.input(1).mf(4).params = [-inp1(4) 0 inp1(4)];
fuzzyDecoded.input(1).mf(5).params = [inp1(1) inp1(3) inp1(7)];
fuzzyDecoded.input(1).mf(6).params = [inp1(2) inp1(6) inp1(8)];
fuzzyDecoded.input(1).mf(7).params = [inp1(5) inp1(9) 10000];

fuzzyDecoded.input(2).mf(1).params = [-10000 -inp2(6) -inp2(4)];
fuzzyDecoded.input(2).mf(2).params = [-inp2(5) -inp2(3) -inp2(1)];
fuzzyDecoded.input(2).mf(4).params = [-inp2(2) 0 inp2(2)];
fuzzyDecoded.input(2).mf(5).params = [inp2(1) inp2(3) inp2(5)];
fuzzyDecoded.input(2).mf(3).params = [inp2(4) inp2(6) 10000];

fuzzyDecoded.output(1).mf(7).params = [-10000 -out1(9) -out1(5)];
fuzzyDecoded.output(1).mf(1).params = [-out1(8) -out1(6) -out1(2)];
fuzzyDecoded.output(1).mf(2).params = [-out1(7) -out1(3) -out1(1)];
fuzzyDecoded.output(1).mf(3).params = [-out1(4) 0 out1(4)];
fuzzyDecoded.output(1).mf(4).params = [out1(1) out1(3) out1(7)];
fuzzyDecoded.output(1).mf(5).params = [out1(2) out1(6) out1(8)];
fuzzyDecoded.output(1).mf(6).params = [out1(5) out1(9) 10000];

end

