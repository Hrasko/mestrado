% DISTRIBUIACO GAUSSIANA
% DESVIO=DESVIO PADRAO
% MI=MEDIA
function [out]=distribuicao_gaussiana(desvio,mi)
    x=0:.001:5;
    t1=1/(desvio*sqrt(2*pi));
    t2=exp(1).^(-1*(x-mi).^2)/(2*desvio)^2;
    y=t1*t2;
    %plot(x,y,'b-');
    out=y(natural_rand(1,length(y)));
end