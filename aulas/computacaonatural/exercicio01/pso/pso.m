%function [out]=pso(func, D, space, mm)
function [out]=pso()
    clc;
    clf;

    %% PARAMETROS
    % NUMERO DE PARTICULAS
    nparticula = 40;
    % FITNESS
    F=zeros(nparticula,1);    
    % ITERACOES
    it=100;
    % CONSTANTE DE ACELERACAO PARA O IBEST
    c1=1.49618;
    % CONSTANTE DE ACELERACAO PARA O GBEST
    c2=c1;
    % PARAMETRO DE DIVERSIFICACAO/EXPLORACAO
    w=0.7298;
    % PARAMETROS W,C1,C2 SUGERIDOS POR POLI R., KENNEDY J. E BLACKWELL T.
    % EM PARTICLE SWARM OPTIMIZATION: A OVERVIEW
    % MELHOR FITNESS
    bestf=0;
    
    % PONTOS DADOS
    inputX=dlmread('X_File.txt');
    inputY=dlmread('Y_File.txt');
    
    % MINIMOS E MAXIMOS DOS PARAMETROS
    kmin=1;
    kmax=5;
    gmin=.01;
    gmax=1;
    dmin=0;
    dmax=50;

    % PARTIUCLAS
    Px=zeros(nparticula,1);
    Pk=zeros(nparticula,1);
    Pg=zeros(nparticula,1);
    Pd=zeros(nparticula,1);
    % MELHORES PARTICULAS INDIVIDUAIS
    Pbestx=zeros(nparticula,1);
    Pbestk=zeros(nparticula,1);
    Pbestg=zeros(nparticula,1);
    Pbestd=zeros(nparticula,1);
    % VELOCIDADE
    Vx=zeros(nparticula,1);
    Vk=zeros(nparticula,1);
    Vg=zeros(nparticula,1);
    Vd=zeros(nparticula,1);
    % MELHOR PARTICULA GLOBAL
    gbestx=0;
    gbestk=0;
    gbestg=0;
    gbestd=0;
   

    %% INICIALIZACAO DAS PARTICULAS
    for i=1:nparticula
        % particulas
        Pk(i)=kmin+(rand*(kmax-kmin));
        Pg(i)=gmin+(rand*(gmax-gmin));
        Pd(i)=dmin+(rand*(dmax-dmin));

        % fitness
        %F(i)=func(Px(i));
        k=Pk(i);
        g=Pg(i);
        d=Pd(i);
        somatorio=0;
        for j=1:length(inputX)
            x=inputX(j);
            y=inputY(j);
            ye=fitness(x,k,g,d);
            e=(y-ye)^2;
            somatorio=somatorio+e;
        end
        F(i)=somatorio;

        % velocidades
        Vx(i)=rand;
        Vk(i)=rand;
        Vg(i)=rand;
        Vd(i)=rand;
        % pbest
        Pbestx(i)=Px(i);
        Pbestk(i)=Pk(i);
        Pbestg(i)=Pg(i);
        Pbestd(i)=Pd(i);
    end

    % LOCALIZANDO GBEST
    bestf=F(1);
    gbestx=Px(1);
    gbestk=Pk(1);
    gbestg=Pg(1);
    gbestd=Pd(1);
    for i=2:nparticula
        if bestf>F(i)
            bestf=F(i);
            gbestx=Px(i);
            gbestk=Pk(i);
            gbestg=Pg(i);
            gbestd=Pd(i);
        end
    end
    
    for i=1:length(inputX)
        x=inputX(i);
        k=gbestk;
        g=gbestg;
        d=gbestd;
        ye=fitness(x,k,g,d);
        Pye(i)=ye;
    end

    %% VIZUALIZACAO DAS ESTIMATIVAS E DADOS
    subplot(2,1,2)
    plot(inputX, inputY,'r.',inputX,Pye,'b.')
    legend('data','estimative')
    hold off
    grid on
    
        
    for i=1:it
        for j=1:nparticula 
            
            % UPDATE VELOCITY
            ibestx=Pbestx(j);
            ibestk=Pbestk(j);
            ibestg=Pbestg(j);
            ibestd=Pbestd(j);
            
            pithx=Px(j);
            Vx(j)=w*Vx(j)+(c1*rand)*(ibestx-pithx)+(c2*rand)*(gbestx-pithx);
            
            pithk=Pk(j);
            Vk(j)=w*Vk(j)+(c1*rand)*(ibestk-pithk)+(c2*rand)*(gbestk-pithk);
            
            pithg=Pg(j);
            Vg(j)=w*Vg(j)+(c1*rand)*(ibestg-pithg)+(c2*rand)*(gbestg-pithg);
            
            pithd=Pd(j);
            Vd(j)=w*Vd(j)+(c1*rand)*(ibestd-pithd)+(c2*rand)*(gbestd-pithd);
                        
            % UPDATE POSITION
            Px(j)=Px(j)+Vx(j);
            Pk(j)=Pk(j)+Vk(j);
            Pg(j)=Pg(j)+Vg(j);
            Pd(j)=Pd(j)+Vg(j);
                        
            % VERIFICANDO SE AS PARTICULAS EXTRAPOLARAM O LIMITE
            if Pk(j)>kmax
                Pk(j)=kmax;
            end
            if Pk(j)<kmin
                Pk(j)=kmin;
            end
            if Pg(j)>gmax
                Pg(j)=gmax;
            end
            if Pg(j)<gmin
                Pg(j)=gmin;
            end
            if Pd(j)>dmax
                Pd(j)=dmax;
            end
            if Pd(j)<dmin
                Pd(j)=dmin;
            end
            
            % UPDATE FITNESS
            k=Pk(j);
            g=Pg(j);
            d=Pd(j);
            somatorio=0;
            fitibest=0;
            fitglobal=0;
            
            for l=1:length(inputX)
                if (mod(j,2) == 0)
                x=inputX(l);
                y=inputY(l);
                ye=fitness(x,k,g,d);
                e=(y-ye)^2;
                somatorio=somatorio+e;

                ye=fitness(x,ibestk,ibestg,ibestd);
                e=(y-ye)^2;
                fitibest=fitibest+e; % fitness do ibest

                ye=fitness(x,gbestk,gbestg,gbestd);
                e=(y-ye)^2;
                fitglobal=fitglobal+e; % fitness do gbest
                end
            end
            F(j)=somatorio;
            
            % UPDATE PBEST
            if F(j)<fitibest
                Pbestx(j)=Px(j);
                Pbestk(j)=Pk(j);
                Pbestg(j)=Pg(j);
                Pbestd(j)=Pd(j);
                % UPDATE GBEST
                if fitibest<fitglobal
                    gbestx=ibestx;
                    gbestk=Pbestk(j);
                    gbestg=Pbestg(j);
                    gbestd=Pbestd(j);
                end
            end
            
        end
        % END DO FOR DAS PARTICULAS
        
        % VIZUALIZACAO DOS PONTOS DO GBEST
        for l=1:length(inputX)
            x=inputX(l);
            k=gbestk;
            g=gbestg;
            d=gbestd;
            ye=fitness(x,k,g,d);
            Pye(l)=ye;
        end

        %% VIZUALIZACAO DAS ESTIMATIVAS E DADOS
        subplot(2,1,2)
        plot(inputX, inputY,'r.',inputX,Pye,'b.')
        legend('data','estimative')
        hold off
        grid on

        Fbest(i)=fitglobal;
        subplot(2,1,1);
        plot([1:i],Fbest,'b-')
        xlabel('iteration')
        ylabel('fitness')
        grid on
        hold on
        drawnow
    end
    somatorio=0;
    %Pye=zeros(length(inputX));
    for i=1:length(inputX)
        x=inputX(i);
        ye=fitness(x,gbestk,gbestg,gbestd);
        y=inputY(i);
        e=(y-ye)^2;
        somatorio=somatorio+e;
        Pye(i)=ye;
    end
    subplot(2,1,2);
    plot(inputX,inputY,'r.',inputX,Pye,'b.')
    legend('data','estimative')
    grid on
    disp(['K=' num2str(gbestk) '; G=' num2str(gbestg) '; D=' num2str(gbestd) '; e=' num2str(somatorio)])
    out=[gbestk gbestg gbestd somatorio];
end