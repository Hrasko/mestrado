function [out]=pso(func, D, space, mm)
    clc;
    clf;

    %% PARAMETROS
    % NUMERO DE PARTICULAS
    nparticula = 40;
    % FITNESS
    F=zeros(nparticula,1);    
    % ITERACOES
    it=100;
    % CONSTANTE DE ACELERACAO PARA O IBEST
    c1=1.49618;
    % CONSTANTE DE ACELERACAO PARA O GBEST
    c2=c1;
    % PARAMETRO DE DIVERSIFICACAO/EXPLORACAO
    w=0.7298;
    % PARAMETROS W,C1,C2 SUGERIDOS POR POLI R. KENNEDY
    % MELHOR FITNESS
    bestf=0;
    

    %% ESPACO DE BUSCA
    if D == 1
        xmin = space(1,1);
        xmax = space(1,2);
        x=xmin:.1:xmax;
        
        % PARTIUCLAS
        Px=zeros(nparticula,1);
        % MELHORES PARTICULAS INDIVIDUAIS
        Pbestx=zeros(nparticula,1);
        % VELOCIDADE
        Vx=zeros(nparticula,1);
        % MELHOR PARTICULA GLOBAL
        gbestx=0;
               
        %% INICIALIZACAO DAS PARTICULAS
        for i=1:nparticula
            Px(i)=xmin+(rand*(xmax-xmin));   
            F(i)=func(Px(i));
            Vx(i)=rand;
            Pbestx(i)=Px(i);
        end
        
        % LOCALIZANDO GBEST
        bestf=F(1);
        gbestx=Px(1);
        for i=2:nparticula
            if mm==0
                if bestf>F(i)
                    bestf=F(i);
                    gbestx=Px(i);
                end
            elseif mm==1
                if bestf<F(i)
                    bestf=F(i);
                    gbestx=Px(i);
                end
            end
        end
        
        
        %% VIZUALIZACAO DA FUNCAO E DAS PARTICULAS
        %subplot(2,2,1); % PRIMEIRO PLOT
        for i=1:length(x)
            Py(i)=func(x(i));
        end
        %subplot(2,1,1);
        plot(x,Py,'b-',Px,F,'ro',gbestx,func(gbestx),'*k')
        grid on
        %hold on
        
        %subplot(2,1,2);
        %stem(0,func(gbestx),'k-')
        %semilogy(0,func(gbestx),'b-')
        %scatter(Px,F)
        %grid on
        %hold on
        
    elseif D==2
        xmin=space(1,1);
        xmax=space(1,2);
        x=xmin:.1:xmax;
        
        ymin=space(2,1);
        ymax=space(2,2);
        y=ymin:.1:ymax;
        
        % PARTICULAS
        Px=zeros(nparticula,1);
        Py=zeros(nparticula,1);
        % MELHORES PARTICULAS INDIVIDUAIS
        Pbestx=zeros(nparticula,1);
        Pbesty=zeros(nparticula,1);
        % VELOCIDADES
        Vx=zeros(nparticula,1);
        Vy=zeros(nparticula,1);
        % MELHOR PARTICULA GLOBAL
        gbestx=0;
        gbesty=0;
        
        % INICIALIZACAO DAS PARTICULAS
        for i=1:nparticula
            Px(i)=xmin+(rand*(xmax-xmin));
            Py(i)=ymin+(rand*(ymax-ymin));
            F(i)=func(Px(i),Py(i));
            Vx(i)=rand;
            Vy(i)=rand;
            Pbestx(i)=Px(i);
            Pbesty(i)=Py(i);
            Pz(i)=func(Px(i),Py(i));
        end
        
        % LOCALIZANDO GBEST
        bestf=F(1);
        gbestx=Px(1);
        gbesty=Py(1);
        for i=2:nparticula
            if mm==0
                if bestf>F(i)
                    bestf=F(i);
                    gbestx=Px(i);
                    gbesty=Py(i);
                end
            elseif mm==1
                if bestf<F(i)
                    bestf=F(i);
                    gbestx=Px(i);
                    gbesty=Py(i);
                end
            end
        end        
        
        [X,Y]=meshgrid(x,y);
        for i=1:length(X)
            for j=1:length(Y)
                Z(i,j)=func(X(i,j),Y(i,j));
            end
        end
        surf(X,Y,Z)
    end
    
        
    for i=1:it
        if mod(i,15)==0
            if D==1
                disp(['Iteration ' num2str(i) ' Gbest=' num2str(func(gbestx))])
            end
        end
        w=(it-i)/it;
        for j=1:nparticula            
            % UPDATE VELOCIDADE
            ibestx=Pbestx(j);
            pithx=Px(j);
            Vx(j)=w*Vx(j)+(c1*rand)*(ibestx-pithx)+(c2*rand)*(gbestx-pithx);
            
            % UPDATE VELOCIDADE EM Y
            if D==2
                ibesty=Pbesty(j);
                pithy=Py(j);
                Vy(j)=w*Vy(j)+(c1*rand)*(ibesty-pithy)+(c2*rand)*(gbesty-pithy);
            end
            
            % UPDATE POSITION
            Px(j)=Px(j)+Vx(j);
            
            % UPDATE POSITION EM Y
            if D==2
                Py(j)=Py(j)+Vy(j);
            end
            
            
            % VERIFICANDO SE AS PARTICULAS NAO EXTRAPOLARAM O LIMITE
            if Px(j)<xmin
                Px(j)=xmin;
            end
            if Px(j)>xmax
                Px(j)=xmax;
            end
            
            if D==2
                if Py(j)<ymin
                    Py(j)=ymin;
                end
                if Py(j)>ymax
                    Py(j)=ymax;
                end
            end
            
            % UPDATE FITNESS
            if D==1
                F(j)=func(Px(j));
            elseif D==2
                F(j)=func(Px(j),Py(j));
            end
            
            % UPDATE PBEST
            if mm==0 % MINIMIZACAO
                if D==1 % BUSCA EM X
                    if F(j)<func(ibestx)
                        Pbestx(j)=Px(j);
                        ibestx=Px(j);
                        % UPDATE GBESTX
                        if func(ibestx)<func(gbestx)
                            gbestx=ibestx;
                        end
                    end
                elseif D==2 % BUSCA EM X,Y
                    if F(j)<func(ibestx,ibesty)
                        Pbestx(j)=Px(j);
                        Pbesty(j)=Py(j);
                        ibestx=Px(j);
                        ibesty=Py(j);
                        % UPDATE GBESTX E GBESTY
                        if func(ibestx,ibesty)<func(gbestx,gbesty)
                            gbestx=ibestx;
                            gbesty=ibesty;
                        end
                    end
                end
            elseif mm==1 % MAXIMIZACAO
                if D==1 % BUSCA EM X
                    if F(j)>func(ibestx)
                        Pbestx(j)=Px(j);
                        ibestx=Px(j);
                        % UPDATE GBEST
                        if func(ibestx)>func(gbestx)
                            gbestx=ibestx;
                        end
                    end
                elseif D==2 % BUSCA EM X,Y  
                    if F(j)>func(ibestx,ibesty)
                        Pbestx(j)=Px(j);
                        Pbesty(j)=Py(j);
                        ibestx=Px(j);
                        ibesty=Py(j);
                        % UPDATE GBESTX E GBESTY
                        if func(ibestx,ibesty)>func(gbestx,gbesty)
                            gbestx=ibestx;
                            gbesty=ibesty;
                        end
                    end
                end
            end
            
            %% VIZUALIZACAO DA FUNCAO E DAS PARTICULAS
            if D==1
                %subplot(2,2,1); % PRIMEIRO PLOT
                %y=zeros(1,length(x));
                for k=1:length(x)
                    Py(k)=func(x(k));
                end
                %subplot(2,1,1);
                plot(x,Py,'b-',Px,F,'ro',gbestx,func(gbestx),'*k')
                grid on
                %hold on
                drawnow
                
                %subplot(2,1,2);
                %plot(Px,F,'ro')
                %semilogy(i,func(gbestx),'b-')
                %stem(i,func(gbestx),'r-')
                %hold on
                %grid on
                %drawnow
                
                
            elseif D==2
                %grid on
                %hold on
                %plot3(X,Y,Z,Px,Py,F)
                %surf(X,Y,Z)
                %drawnow
            end
        end
    end
    if D==1
        disp(['Iteration ' num2str(it) ' Gbest=' num2str(func(gbestx))])
        out=[gbestx func(gbestx)];
    elseif D==2
        out=[gbestx,gbesty,func(gbestx,gbesty)];
    end
end