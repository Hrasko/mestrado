function [nIN nOUT IN OUT] = processControlData( fileName )
%PROCESSCONTROLDATA Summary of this function goes here
%   Detailed explanation goes here

all = dlmread(fileName);

speed = all(:,1);

[ nINpr nOUTpr pr out] = predictionConversion(speed,3,1);

cut = nINpr+nOUTpr:size(speed,1);

waypoints = all(cut,2:7);

raycasting = all(cut,8:14);

OUT = all(cut,15:16);

IN = [pr out waypoints raycasting];

nIN = size(IN,2);

nOUT = size(OUT,2);

end

