clear;clc;

datas;

%iris
 nIN = 4; nOUT = 3; 
 in = normc(irisIN)';
 out = normc(irisOUT)';

%wine
%nIN = 13; nOUT = 3;
%in = normc(wineIN)';
%out = normc(wineOUT)';

%cancer
% nIN = 9; nOUT = 1;
% in = normc(cancerIN)';
% out = normc(cancerOUT)';
% 
nn = nIN*4;
nodes = [nIN nn nn nn nOUT];


full = [in out];
num = size(full,1);
num70 = round(num*0.7);

maxInt = 10;
erroBaseInteira = 1:maxInt;
percBaseInteira = 1:maxInt;
erroBasePrev = 1:maxInt;
percBasePrev = 1:maxInt;

for iteraction = 1:maxInt
    display(erroBaseInteira);
    display(percBaseInteira);
    display(erroBasePrev);
    display(percBasePrev);
    display(iteraction);
    
    idx = randperm(num);
    full = full(idx,:);
    
    INFull = full(:,1:nIN);
    
    OUTFull = full(:,nIN+1:nIN+nOUT);

    INTrain = INFull(1:num70,:);
    OUTTrain = OUTFull(1:num70,:);
    INPrev = INFull(num70:num,:);
    OUTPrev = OUTFull(num70:num,:);

    IN = INTrain;
    OUT = OUTTrain;
    
    dnn = randDBN( nodes, 'GBDBN' );
    nrbm = numel(dnn.rbm);

    opts.MaxIter = 500;
    opts.BatchSize = round(num/4);
    opts.Verbose = false;
    opts.StepRatio = 0.01;
    opts.Layer = nrbm-1;
    opts.DropOutRate = 0.5;
    opts.Object = 'Square';

    dnn = pretrainDBN(dnn, IN, opts);
    dnn= SetLinearMapping(dnn, IN, OUT);

    opts.Layer = 0;
    opts.MaxIter = 10;

    dnn = trainDBN(dnn, IN, OUT, opts);
    
    [erroBaseInteira(iteraction) percBaseInteira(iteraction)] = classificationError(dnn,INFull,OUTFull);
    [erroBasePrev(iteraction) percBasePrev(iteraction)] = classificationError(dnn,INPrev,OUTPrev);
    clc;
end

display(erroBaseInteira);
display(percBaseInteira);
display(erroBasePrev);
display(percBasePrev);

eb = sum(erroBaseInteira)/size(erroBaseInteira,2);
ep = sum(erroBasePrev)/size(erroBasePrev,2);

display(eb);
display(ep);

%wine
% dlmwrite('ebi_wine.txt',erroBaseInteira);
% dlmwrite('pbi_wine.txt',percBaseInteira);
% dlmwrite('ebp_wine.txt',erroBasePrev);
% dlmwrite('pbp_wine.txt',percBasePrev);

%cancer
dlmwrite('erroBaseInteira.txt',erroBaseInteira);
dlmwrite('percBaseInteira.txt',percBaseInteira);
dlmwrite('erroBasePrev.txt',erroBasePrev);
dlmwrite('percBasePrev.txt',percBasePrev);
