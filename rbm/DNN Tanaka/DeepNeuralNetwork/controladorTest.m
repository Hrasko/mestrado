clear;clc;

datas;

resultado = [];
best = 1000000;
bestDelay = 1;
bestDelta = 1;
contador = 0;



intervalo = 1;%round(0.0063*size(db,1));
atraso = 4;%round(0.0168*size(db,1));
%for intervalo = 10:30
for atraso = 10:30


contador = contador+1;
 [nIN nOUT in out] = predictionConversion(db,atraso,intervalo);
 
 [in maxIN minIN] = normM(in);
 [out maxOUT minOUT] = normM(out);

errorFunction = @predictionErrorRBMFF;
%iris
%   nIN = 4; nOUT = 3; 
%   in = normM(irisIN');
%   out = normM(irisOUT');

%wine
%nIN = 13; nOUT = 3;
%in = normc(wineIN)';
%out = normc(wineOUT)';

%cancer
%nIN = 9; nOUT = 1;
%in = normc(cancerIN)';
%out = normc(cancerOUT)';

nn = size(in,1);
nodes = [nIN 100 50];

full = [in out];
unshuffled = full;
num = size(full,1);
num70 = round(num*0.7);

maxInt = 10;
erroBaseInteira = 1:maxInt;
percBaseInteira = 1:maxInt;
erroBasePrev = 1:maxInt;
percBasePrev = 1:maxInt;
bestDBNIteractionError = 10000000;
for iteraction = 1:maxInt  
    display(iteraction);
    display(contador);
display(atraso);
    
    %shuffle
    % idx = randperm(num);
    % full = full(idx,:);
    
    INFull = full(:,1:nIN);
    INUnshuffled = unshuffled(:,1:nIN);
    OUTFull = full(:,nIN+1:nIN+nOUT);
    OUTUnshuffled = unshuffled(:,nIN+1:nIN+nOUT);

    INTrain = INFull(1:num70,:);
    OUTTrain = OUTFull(1:num70,:);
    INPrev = INFull(num70:num,:);
    OUTPrev = OUTFull(num70:num,:);

    IN = INTrain;
    OUT = OUTTrain;
    
    dnn = randDBN( nodes, 'GBDBN' );
    nrbm = numel(dnn.rbm);

    opts.MaxIter = nn;
    opts.BatchSize = round(num/4);
    opts.Verbose = false;
    opts.StepRatio = 0.01;
    %opts.Layer = nrbm-1;
    opts.DropOutRate = 0.5;
    opts.Object = 'Square';

    dnn = pretrainDBN(dnn, IN, opts);
    %dnn= SetLinearMapping(dnn, IN, OUT);

    %opts.Layer = 1;
    %opts.MaxIter = 100;

    %dnn = trainDBN(dnn, IN, OUT, opts);
    
    outDNN = v2h(dnn,IN);
    
    net = feedforwardnet(50);    
    net = configure(net,outDNN',OUT');
    net = train(net,outDNN',OUT');
    
    [erroBaseInteira(iteraction) percBaseInteira(iteraction)] = errorFunction(dnn,net, INFull, OUTFull);%classificationError(dnn,INFull,OUTFull);
    
    [erroBasePrev(iteraction) percBasePrev(iteraction)] = errorFunction (dnn,net, INPrev, OUTPrev);%predictionError(dnn, INPrev, OUTPrev);%classificationError(dnn,INPrev,OUTPrev);    
    
    if (bestDBNIteractionError > erroBasePrev(iteraction))
       bestDBNIteractionError = erroBasePrev(iteraction);
       bestDNN = dnn;
       
    end
end

eb = sum(erroBaseInteira)/size(erroBaseInteira,2);
ep = sum(erroBasePrev)/size(erroBasePrev,2);

resultados(contador) = (eb+ep);

if (resultados(contador) < best)
   best = eb+ep;
   bestDelta = intervalo;
   bestDelay = atraso;
   
   outDNN = v2h(dnn,INUnshuffled);
   out = net(outDNN')';
   
   h = figure; 
   
   predOnly = size(OUTTrain,1):size(OUTFull,1);
   original = denormM(OUTUnshuffled,maxOUT,minOUT);
   out = denormM(out,maxOUT,minOUT);
   plot(1:size(OUTTrain,1),original(1:size(OUTTrain,1)),'-',predOnly,original(predOnly),'r-',predOnly,out(predOnly),'g-');
   legend('Training','Comparison Data','NN Prediction');
   saveas(h,'bestPrediction','jpg')
    dlmwrite('out.txt',out);
    save bestDNNAll bestDNN;
   dlmwrite('erroBaseInteira.txt',erroBaseInteira);
   dlmwrite('percBaseInteira.txt',percBaseInteira);
   dlmwrite('erroBasePrev.txt',erroBasePrev);
   dlmwrite('percBasePrev.txt',percBasePrev);
   
end

dlmwrite('resultados.txt',resultados);
dlmwrite('melhorDelta.txt',bestDelta);
dlmwrite('melhorDelay.txt',bestDelay);


%     end
 end
