clear;clc;

%loading in mem all our databases
datas;

%which metric function should we use
errorFunction = @classificationErrorRBMFF;

%%Wine data
% number of in and out
nIN = 13; nOUT = 3;
in = normM(wineIN');
out = wineOUT';

% number of neurons in the last layer
nn = nIN*4;

% run and save the data
runDBN;
   
% saving all metric
dlmwrite('erroBaseInteira.txt',erroBaseInteira);
dlmwrite('percBaseInteira.txt',percBaseInteira);
dlmwrite('erroBasePrev.txt',erroBasePrev);
dlmwrite('percBasePrev.txt',percBasePrev);
