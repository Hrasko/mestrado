%  This script requires ome previous preparation to work. Check other
%  scripts (Iris.m) for more information

% DBN node information. 
% nn is the number of neurons in the feedfoward layer
nodes = [nIN nn*4 nn*2 nn 2];

% Matrix with all database. This is used for shuffleing
full = [in out];

% saving the unshuffled Matrix for posterious use
unshuffled = full;

% Size of the sampling
num = size(full,1);

%70% of the sampling
num70 = round(num*0.7);

%how many times we will check the algorithm
maxInt = 10;

%Used for metrics
erroBaseInteira = ones(maxInt,2);
percBaseInteira = erroBaseInteira;
erroBasePrev = erroBaseInteira;
percBasePrev = erroBaseInteira;

for iteraction = 1:maxInt
    
    %Displaying current samples
    clc;
    display(iteraction);    
    display(erroBaseInteira);
    display(erroBasePrev);    
    display(percBaseInteira);
    display(percBasePrev);
    
    %shuffle
    idx = randperm(num);
    full = full(idx,:);
    
    % Separating the data in more useful matrix
    INFull = full(:,1:nIN);
    INUnshuffled = unshuffled(:,1:nIN);
    OUTFull = full(:,nIN+1:nIN+nOUT);
    OUTUnshuffled = unshuffled(:,nIN+1:nIN+nOUT);
    INTrain = INFull(1:num70,:);
    OUTTrain = OUTFull(1:num70,:);
    INPrev = INFull(num70:num,:);
    OUTPrev = OUTFull(num70:num,:);
    IN = INFull;
    OUT = OUTFull;
    
    %DBN initiation
    dnn = randDBN( nodes, 'GBDBN' );
    nrbm = numel(dnn.rbm);
    opts.MaxIter = 1000;
    opts.BatchSize = round(num/4);
    opts.Verbose = false;
    opts.Layer = nrbm-1;
    opts.StepRatio = 0.01;    
    opts.DropOutRate = 0.5;
    opts.Object = 'Square';

    %Training the DBN layers
    dnn = pretrainDBN(dnn, IN, opts);
    
    %Sampling the DBN result
%     outDNN = v2h(dnn,IN);
    
    %FeedFoward trainign with Backpropagation
%     net = feedforwardnet(nn);    
%     net = configure(net,outDNN',OUT');
%     net = train(net,outDNN',OUT');

    dnn= SetLinearMapping(dnn, IN, OUT);

    opts.Layer = 0;
    opts.MaxIter = 10;

    dnn = trainDBN(dnn, IN, OUT, opts);
    net = 0;
    %Saving the metrics
    [erroBaseInteira(iteraction,:) percBaseInteira(iteraction,:)] = errorFunction(dnn,net, INFull, OUTFull);
    [erroBasePrev(iteraction,:) percBasePrev(iteraction,:)] = errorFunction (dnn,net, INPrev, OUTPrev);    
    
end

% resultados(contador) = (eb+ep);
% 
% if (resultados(contador) < best)

   
   outDNN = v2h(dnn,INUnshuffled);
   out = net(outDNN')';
   
%    h = figure; 
%    
%    predOnly = size(OUTTrain,1):size(OUTFull,1);
%    original = denormM(OUTUnshuffled,maxOUT,minOUT);
%    %out = denormM(out,maxOUT,minOUT);
%    plot(1:size(OUTTrain,1),original(1:size(OUTTrain,1)),'-',predOnly,original(predOnly),'r-',predOnly,out(predOnly),'g-');
%    legend('Training','Comparison Data','NN Prediction');
%    saveas(h,'bestPrediction','jpg')
    
   dlmwrite('erroBaseInteira.txt',erroBaseInteira);
   dlmwrite('percBaseInteira.txt',percBaseInteira);
   dlmwrite('erroBasePrev.txt',erroBasePrev);
   dlmwrite('percBasePrev.txt',percBasePrev);
% end

% dlmwrite('resultados.txt',resultados);
% dlmwrite('melhorDelta.txt',bestDelta);
% dlmwrite('melhorDelay.txt',bestDelay);


%     end
% end
