clear;clc;

%which metric function should we use
errorFunction = @predictionErrorRBMFF;

%%Wp3 data
%Creating the delay 
[nIN nOUT IN OUT] = processControlData( 'wp3.txt' );

%normalization
[in maxIN minIN] = normM(IN);
[out maxOUT minOUT] = normM(OUT);

% number of neurons in the last layer
nn = 1000;

% run and save the data
runDBN;
   
% saving all metric
dlmwrite('erroBaseInteira.txt',erroBaseInteira);
dlmwrite('percBaseInteira.txt',percBaseInteira);
dlmwrite('erroBasePrev.txt',erroBasePrev);
dlmwrite('percBasePrev.txt',percBasePrev);

%Sampling the unshuffled data from DBN layers
outDNN = v2h(dnn,INUnshuffled);
% Result of the feedfowardnet given the result of the DBN
outNet = net(outDNN')';

%saving an image
h = figure; 
predOnly = size(OUTTrain,1):size(OUTFull,1);
original = denormM(OUTUnshuffled,maxOUT,minOUT);
outNet = denormM(outNet,maxOUT,minOUT);
plot(1:size(OUTTrain,1),original(1:size(OUTTrain,1)),'-',predOnly,original(predOnly),'r-',predOnly,outNet(predOnly),'g-');
legend('Training','Comparison Data','NN Prediction');
saveas(h,'bestPrediction','jpg');