function [nIN nOUT in out] = processaLoteria( data,nPossiveis )
%PROCESSALOTERIA Summary of this function goes here
%   Detailed explanation goes here
for i=1:size(data,1)
   data(i,:) = sort(data(i,:)); 
end
atraso = 3;
in1 =data(1:size(data,1)-3,:);
in2 =data(2:size(data,1)-2,:);
in3 =data(3:size(data,1)-1,:);

in = [in1 in2 in3];


out = zeros(size(data,1)-atraso,nPossiveis);
for i=atraso+1:size(data,1)-atraso
    for j=1:size(data,2)
       out(i-atraso,data(i,j)) = 1;
    end
end

nIN = size(in,2);
nOUT = size(out,2);
end

