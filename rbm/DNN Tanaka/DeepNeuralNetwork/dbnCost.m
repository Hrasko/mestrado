function [y dnn net]=dbnCost(pos,db)
    
    delay = round(pos(1));
    delta = round(pos(2));
    
    nn = 50;
    


%Creating the delay 
[nIN nOUT in out] = predictionConversion(db,delay,delta);

%normalization
[in maxIN minIN] = normM(in);
[out maxOUT minOUT] = normM(out);

% run and save the data   
[y dnn net] = runDBN(nn,nIN,nOUT,in,out);

nome1 = strcat(num2str(y),'x',num2str(delay),'x',num2str(delta),'DNN.mat');
save(nome1,'dnn');
nome2 = strcat(num2str(y),'x',num2str(delay),'x',num2str(delta),'NET.mat');
save(nome2,'net');

end