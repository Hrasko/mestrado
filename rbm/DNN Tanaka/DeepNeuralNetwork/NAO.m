clear;clc;

%loading in mem all our databases
datas;

%which metric function should we use
errorFunction = @predictionErrorRBMFF;

%%EneryAus data
% number of in and out
db = NAO;
delta = 4;
delay = 13;

%Creating the delay 
[nIN nOUT in out] = predictionConversion(db,delay,delta);

%normalization
[in maxIN minIN] = normM(in);
[out maxOUT minOUT] = normM(out);

% number of neurons in the last layer
nn = 50;

% run and save the data
runDBN;
   
% saving all metric
dlmwrite('erroBaseInteira.txt',erroBaseInteira);
dlmwrite('percBaseInteira.txt',percBaseInteira);
dlmwrite('erroBasePrev.txt',erroBasePrev);
dlmwrite('percBasePrev.txt',percBasePrev);

%Sampling the unshuffled data from DBN layers
outDNN = v2h(dnn,INUnshuffled);
% Result of the feedfowardnet given the result of the DBN
outNet = net(outDNN')';

%saving an image
h = figure; 
predOnly = size(OUTTrain,1):size(OUTFull,1);
original = denormM(OUTUnshuffled,maxOUT,minOUT);
outNet = denormM(outNet,maxOUT,minOUT);
plot(1:size(OUTTrain,1),original(1:size(OUTTrain,1)),'-',predOnly,original(predOnly),'r-',predOnly,outNet(predOnly),'g-');
legend('Training','Comparison Data','NN Prediction');
saveas(h,'bestPrediction','jpg');