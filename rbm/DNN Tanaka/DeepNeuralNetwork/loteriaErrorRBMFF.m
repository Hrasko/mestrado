function [ err avg ] = loteriaErrorRBMFF( dnn,IN,OUT )

out = v2h(dnn,IN);

[sizeData numberCluster] = size(OUT);

err = 0;

for i = 1:sizeData
   [lixo r] = sort(out(i,:));
   for j=1:numberCluster
       if OUT(i,r(j)) ~= 1
          err = err + 1;
       end
   end
end

avg = err/sizeData;

end

