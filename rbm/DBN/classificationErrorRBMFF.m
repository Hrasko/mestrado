function [ err avg ] = classificationErrorRBMFF( dnn,net,IN,OUT )

outDNN = v2h(dnn,IN);
out = net(outDNN')';

[sizeData numberCluster] = size(OUT);

err = 0;

for i = 1:sizeData
   m = out(i,1);
   t = 1;
   for j=2:numberCluster
       if out(i,j) > m
          m = out(i,j);
          t = j;
       end
   end
   if OUT(i,t) ~= 1
      err = err +1; 
   end
end

avg = err/sizeData;

end

