function [y dnn net]=dbnCost(pos)
db = dlmread ('cyrela');
    delay = round(pos(1));
    delta = round(pos(2));
    
    nn = 50;
    


%Creating the delay 
[nIN nOUT in out] = predictionConversion(db,delay,delta);

%normalization
[in maxIN minIN] = normM(in);
[out maxOUT minOUT] = normM(out);

% run and save the data   
[y dnn net] = runDBN(nn,nIN,nOUT,in,out);

nome1 = strcat('dnn',num2str(delay),'x',num2str(delta),'x',num2str(y),'.mat');
save(nome1,'dnn');
nome2 = strcat('net',num2str(delay),'x',num2str(delta),'x',num2str(y),'.mat');
save(nome2,'net');

end