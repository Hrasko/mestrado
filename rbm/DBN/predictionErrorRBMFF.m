function [ eqm dev ] = predictionErrorRBMFF( dnn,net,IN,OUT )
%PREDICTIONERROR Summary of this function goes here
%   Detailed explanation goes here
outDNN = v2h(dnn,IN);
out = net(outDNN')';
err = power(OUT-out,2);
[n N] = size(err);
eqm = sum(err)/n;
dev = std((OUT-out)/n);

end

