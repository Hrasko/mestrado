function [ X] = denormM( X_norm,mu,sigma )
%NORMALIZE Summary of this function goes here
%   Detailed explanation goes here

X = X_norm;
mu = zeros(1, size(X, 2));
sigma = zeros(1, size(X, 2));

for i = 1:size(X,2)
    X(:,i) = X_norm(:,i) * sigma(i);
    X(:,i) = X_norm(:,i) + mu(i);    
end