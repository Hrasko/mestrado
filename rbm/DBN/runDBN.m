function [errorOut dnn net] = runDBN(nn,nIN,nOUT,in,out)


% DBN node information. 
% nn is the number of neurons in the feedfoward layer
nodes = [nIN nn*2 nn];

% Matrix with all database. This is used for shuffleing
full = [in out];

% saving the unshuffled Matrix for posterious use
unshuffled = full;

% Size of the sampling
num = size(full,1);

%70% of the sampling
num70 = round(num*0.7);

if num <= 0
    errorOut = 10000000;
    dnn = 0;
    net = 0;
   return;
end
   
if num70 <= 0
    errorOut = 10000000;
    dnn = 0;
    net = 0;
   return;
end
    %shuffle
    idx = randperm(num);
    full = full(idx,:);
    
    % Separating the data in more useful matrix
    INFull = full(:,1:nIN);
    %INUnshuffled = unshuffled(:,1:nIN);
    OUTFull = full(:,nIN+1:nIN+nOUT);
    %OUTUnshuffled = unshuffled(:,nIN+1:nIN+nOUT);
    INTrain = INFull(1:num70,:);
    OUTTrain = OUTFull(1:num70,:);
    INPrev = INFull(num70:num,:);
    OUTPrev = OUTFull(num70:num,:);
    IN = INTrain;
    OUT = OUTTrain;
    
    %DBN initiation
    dnn = randDBN( nodes, 'GBDBN' );
    nrbm = numel(dnn.rbm);
    opts.MaxIter = 1000;
    opts.BatchSize = round(num/4);
    opts.Verbose = false;
    opts.StepRatio = 0.01;    
    opts.DropOutRate = 0.5;
    opts.Object = 'Square';

    %Training the DBN layers
    dnn = pretrainDBN(dnn, IN, opts);
    
    %Sampling the DBN result
    outDNN = v2h(dnn,IN);
    
    %FeedFoward trainign with Backpropagation
    net = feedforwardnet(nn);    
    net = configure(net,outDNN',OUT');
    net = train(net,outDNN',OUT');
    
    [errorOut lixo] = predictionErrorRBMFF (dnn,net, INPrev, OUTPrev);    


end
