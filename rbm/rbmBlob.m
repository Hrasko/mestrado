classdef rbmBlob < handle
    %rbmBlob adapted from https://github.com/echen/restricted-boltzmann-machines/blob/master/README.md
    
    properties
        num_hidden = 1;
        num_visible = 1;
    	learning_rate = 0.2;
        weights = [];
    end
    
    methods
        function rbmBlob = setup(rbmBlob, num_visible, num_hidden, learning_rate)
            rbmBlob.num_visible = num_visible;
            rbmBlob.num_hidden = num_hidden;
            rbmBlob.learning_rate = learning_rate;
            % pesos iniciais randomizados
            rbmBlob.weights = randn(num_visible+1, num_hidden+1);
            rbmBlob.weights(1,:) = 0;
            rbmBlob.weights(:,1) = 0;
        end
        function rbmBlob = train(rbmBlob,data, maxEpoch)            
            num_examples = size(data,1);
            
            % Insert bias units of 1 into the first column.
            bias = ones(size(data,1),1);
            data = [bias data];
            
            for epoch = 1:maxEpoch
                % Clamp to the data and sample from the hidden units. (This is the "positive CD phase", aka the reality phase.)
                pos_hidden_activations = data * rbmBlob.weights;
                pos_hidden_probs = sigmf(pos_hidden_activations,[1 0]);
                pos_hidden_states = pos_hidden_probs > rand(num_examples,rbmBlob.num_hidden + 1);
                % Note that we're using the activation *probabilities* of the hidden states, not the hidden states themselves, when computing associations. We could also use the states; see section 3 of Hinton's  "A Practical Guide to Training Restricted Boltzmann Machines" for more.
                pos_associations = data' * pos_hidden_probs;
                
                % Reconstruct the visible units and sample again from the hidden units. (This is the "negative CD phase", aka the daydreaming phase.)
                neg_visible_activations = pos_hidden_states * rbmBlob.weights';
                neg_visible_probs = sigmf(neg_visible_activations,[1 0]);
                neg_visible_probs(:,1) = 1; %Fix the bias unit.
                neg_hidden_activations = neg_visible_probs * rbmBlob.weights;
                neg_hidden_probs = sigmf(neg_hidden_activations,[1 0]);
                % Note, again, that we're using the activation *probabilities* when computing associations, not the states themselves.
                neg_associations = neg_visible_probs' * neg_hidden_probs;

                % Update weights.
                rbmBlob.weights = rbmBlob.weights + rbmBlob.learning_rate * ((pos_associations - neg_associations) / num_examples);

                e = sum(sum(data - neg_visible_probs)) .^ 2;
                fprintf('Epoch %d: error is %f \n',epoch, e);
                %pause(1);
            end
        end
        
        function [rbmBlob,visible_states] = run_hidden(rbmBlob, data)
            
            % Assuming the rbmBlob has been trained (so that weights for the network have been learned),
            % run the network on a set of hidden units, to get a sample of the visible units.

            num_examples = size(data,1);

            % Create a matrix, where each row is to be the visible units (plus a bias unit)
            % sampled from a training example.
            rbmBlob.visible_states = ones(num_examples, rbmBlob.num_visible + 1);

            % Insert bias units of 1 into the first column of data.
            bias = ones(size(data,1),1);
            data = [bias data];

            %Calculate the activations of the visible units.
            visible_activations = data * rbmBlob.weights';
            % Calculate the probabilities of turning the visible units on.
            visible_probs = sigmf(visible_activations,[1 0]);
            % Turn the visible units on with their specified probabilities.
            visible_states(:,:) = visible_probs > rand(num_examples, rbmBlob.num_visible + 1);
            % Always fix the bias unit to 1.
            visible_states(:,1) = 1;

            % Ignore the bias units.
            visible_states = visible_states(:,2:size(visible_states,2));
        
        end
    
        function [rbmBlob,hidden_states] = run_visible(rbmBlob, data)
            
            % Assuming the rbmBlob has been trained (so that weights for the network have been learned),
            % run the network on a set of hidden units, to get a sample of the visible units.

            num_examples = size(data,1);

            % Create a matrix, where each row is to be the visible units (plus a bias unit)
            % sampled from a training example.
            hidden_states = ones(num_examples, rbmBlob.num_hidden + 1);

            % Insert bias units of 1 into the first column of data.
            bias = ones(size(data,1),1);
            data = [bias data];

            %Calculate the activations of the visible units.
            hidden_activations = data * rbmBlob.weights;
            % Calculate the probabilities of turning the visible units on.
            hidden_probs = sigmf(hidden_activations,[1 0]);
            % Turn the visible units on with their specified probabilities.
            hidden_states(:,:) = hidden_probs > rand(num_examples, rbmBlob.num_hidden + 1);
            % Always fix the bias unit to 1.
            hidden_states(:,1) = 1;

            % Ignore the bias units.
            hidden_states = hidden_states(:,2:size(hidden_states,2));
        
        end
        
    end
end
