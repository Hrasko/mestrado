clear;clc;
%r = rbmBlob;
%r.setup(2,2,0.1);
%training_data = [[1,1];[1,0];[0,1];[0,0]];
%r.train(training_data,1000);
%[r,x] = r.run_visible([[1,1];[1,0];[0,1];[0,0]]);
%display(x);

net = fitnet(10);


P = [0 0 1 1; 1 0 1 0];
T = [0 0 1 1];
net = configure(net,P,T);
view(net);

[net,tr] = train(net,P,T);

y1 = net(P);
plot(P,T,'o',P,y1,'x');