classdef rbm < handle
    %RBMPCD based on http://deeplearning.net/tutorial/rbm.html
    
    properties
        input = [];
        n_visible=784;
        n_hidden=500;
        W=[];
        hbias=[];
        vbias=[];
    end
    
    methods
        %function [rbm,a,b] = test(rbm)
        %    a = 1;
        %    b = 2;
        %end
        
        function rbm = init(rbm, input, n_visible, n_hidden, W, hbias, vbias)
            %RBM "constructor". Defines the parameters of the model along with
            %basic operations for inferring hidden from visible (and vice-versa),
            %as well as for performing CD updates.

            %:param input: None for standalone RBMs or symbolic variable if RBM is
            %part of a larger graph.

            %:param n_visible: number of visible units

            %:param n_hidden: number of hidden units

            %:param W: None for standalone RBMs or symbolic variable pointing to a
            %shared weight matrix in case RBM is part of a DBN network; in a DBN,
            %the weights are shared between RBMs and layers of a MLP

            %:param hbias: None for standalone RBMs or symbolic variable pointing
            %to a shared hidden units bias vector in case RBM is part of a
            %different network

            %:param vbias: None for standalone RBMs or a symbolic variable
            %pointing to a shared visible units bias
            if exist('n_visible', 'var')
                rbm.n_visible = n_visible;
            end
            if exist('n_hidden', 'var')
                rbm.n_hidden = n_hidden;
            end

            if ~exist('W', 'var')
                % W is initialized with `initial_W` which is uniformely
                % sampled from -4*sqrt(6./(n_visible+n_hidden)) and
                % 4*sqrt(6./(n_hidden+n_visible))
            
                low = -4 * sqrt(6. / (rbm.n_hidden + rbm.n_visible));
                high = 4 * sqrt(6. / (rbm.n_hidden + rbm.n_visible));

                rbm.W = rand(rbm.n_visible, rbm.n_hidden)*(high+low) + low;
            else
                rbm.W = W;
            end            
            
            %bias for hidden layer
            if ~exist('hbias', 'var')
               rbm.hbias = zeros(rbm.n_hidden,1);
            else
               rbm.hbias =  hbias;
            end
            
            %bias for visible layer
            if ~exist('vbias', 'var')
               rbm.vbias = zeros(rbm.n_visible,1);
            else
               rbm.vbias =  vbias;
            end
            
            % initialize input layer for standalone RBM or layer0 of DBN
            rbm.input = input;
        end
        
        function [rbm,pre_sigmoid_activation,sigmoid] = propup(rbm, vis)
            %This function propagates the visible units activation upwards to the hidden units
            %Note that we return also the pre-sigmoid activation of the layer.
            pre_sigmoid_activation = dot(rbm.W, vis) + rbm.hbias;
            sigmoid = sigmf(pre_sigmoid_activation);
        end
        
        function [rbm,pre_sigmoid_h1, h1_mean, h1_sample] = sample_h_given_v(rbm,v0_sample)
            %This function infers state of hidden units given visible units
            % compute the activation of the hidden units given a sample of the visibles
            [pre_sigmoid_h1, h1_mean] = rbm.propup(v0_sample);
            % get a sample of the hiddens given their activation. 
            h1_sample = binornd(1, h1_mean);
        end
        
        function [rbm,pre_sigmoid_activation,sigmoid] = propdown(rbm, hid)
            %This function propagates the visible units activation upwards to the hidden units
            %Note that we return also the pre-sigmoid activation of the layer.
            pre_sigmoid_activation = dot(rbm.W', hid) + rbm.vbias;
            sigmoid = sigmf(pre_sigmoid_activation);
        end
        
        function [rbm,pre_sigmoid_h1, v1_mean, v1_sample] = sample_v_given_h(rbm,h0_sample)
            %This function infers state of hidden units given visible units
            % compute the activation of the hidden units given a sample of the visibles
            [pre_sigmoid_h1, v1_mean] = rbm.propdown(h0_sample);
            % get a sample of the hiddens given their activation. 
            v1_sample = binornd(1, v1_mean);
        end
        
        function [rbm,pre_sigmoid_v1, v1_mean, v1_sample,pre_sigmoid_h1, h1_mean, h1_sample] = gibbs_hvh(rbm, h0_sample)
            %This function implements one step of Gibbs sampling, starting from the hidden state
            [pre_sigmoid_v1, v1_mean, v1_sample] = rbm.sample_v_given_h(h0_sample);
            [pre_sigmoid_h1, h1_mean, h1_sample] = rbm.sample_h_given_v(v1_sample);
        end
        
        function [rbm,pre_sigmoid_h1, h1_mean, h1_sample,  pre_sigmoid_v1, v1_mean, v1_sample] = gibbs_vhv(rbm, v0_sample)
            %This function implements one step of Gibbs sampling, starting from the visible state
            [pre_sigmoid_h1, h1_mean, h1_sample] = rbm.sample_h_given_v(v0_sample);
            [pre_sigmoid_v1, v1_mean, v1_sample] = rbm.sample_v_given_h(h1_sample);           
        end
        
        function [rbm,result] = free_energy(rbm, v_sample)
           wx_b = dot(rbm.W,v_sample ) + rbm.hbias;
           vbias_term = dot(rbm.vbias,v_sample);
           hidden_term = sum(log(1+exp(wx_b));
           result = -vbias_term - hidden_term;
        end      
        
    end
    
end

