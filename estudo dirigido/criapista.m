clear ; close all; clc

targets = rand(10,2);

track = zeros(21,2);

targets(1,:) = targets(1,:) + ones(1,2);

for i=2:10
   track(i,:) = track(i-1,:) + targets(i,:);
end

targets = rand(10,2);

for i=11:20
   track(i,:) = track(i-1,:) - targets(i-10,:);
end

track = track;

plot(track(:,1),track(:,2),'->');