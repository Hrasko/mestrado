function [ updatedPos ] = simuCar( previousPos, u, v )
%SIMUCAR Summary of this function goes here
%   Detailed explanation goes here
    updatedPos = 1:3;
    
    theta = previousPos(1);
    if (rand > 0.01)
        updatedPos(1) = previousPos(1) + tan(u);    
    end
    
    updatedPos(2) = previousPos(2) + v*cos(theta);
    updatedPos(3) = previousPos(3) + v*sin(theta);
end

