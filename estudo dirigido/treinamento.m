clear ; close all; clc; pause on;
warning ('off','all');
% inicializa
track = dlmread('track2.txt');
track = track*100;

resultados = zeros(30,3);

for i = 1:30
    myfuzzy = readfis('teste4.fis');
    fprintf('Execucao %d \n',i);
    [ best,bestFit,interactions,BestCounter,bestCar ] = psoModuleLBest(50,100,31,1.49445, 1.49445, 0.729,@fuzzyCost,[15 20 1] , myfuzzy, track);
    
    resultados(i,1) = bestFit;
    resultados(i,2) = BestCounter;
    resultados(i,3) = interactions;
    myfuzzy2 = decodeFuzzy(best,myfuzzy);
    writefis(myfuzzy2,strcat('treinado',num2str(i)));
    dlmwrite('resultados.txt',resultados);
    
    
end

print('done');
warning ('on','all');