function [ cost,carTracker ] = fuzzyCost( pos, p ,myFuzzy, track)
%FCPCOST Summary of this function goes here
%   Detailed explanation goes here

maxTime = 1000;
carTracker = zeros(maxTime,3);

dCost = 0;
triggeredPoints = 1;
checkpoint = 1;
checkpointZoneTrigger = p(1);
carMaxSpeed= p(2);
acceleration= p(3);
trackSize = size(track,1);

myfuzzy = decodeFuzzy(pos,myFuzzy, p);

currentSpeed = 0;

for i = 2:maxTime
   
    posicaoAtual = carTracker(i-1,:);    
    target = track(checkpoint,:);
    
    % se o checkpoint é o ultimo ponto, o proximo é o primeiro da lista
    if checkpoint+1 > trackSize
        nextTarget = track(1,:);
    else
        nextTarget = track(checkpoint+1,:);
    end
    
    % calcula distancia euclidiana ate o checkpoint
    X = [posicaoAtual(2),posicaoAtual(3);target(1),target(2)];
    d = pdist(X,'euclidean');
    
    % distancia acumulada
    dCost = dCost + d;
    
    % se estiver perto suficiente, consideramos que atravessou o checkpoint
    if d <= checkpointZoneTrigger
        triggeredPoints = triggeredPoints +1;
       checkpoint = checkpoint +1;
       if (checkpoint > trackSize)
           break;
       end
    end    
    
    % pontos para a reta que define esq ou dir
    x1 = posicaoAtual(2);
    y1 = posicaoAtual(3);
    x2 = posicaoAtual(2) + cos(posicaoAtual(1));
    y2 = posicaoAtual(3) + sin(posicaoAtual(1));
    
    %calcula angulos pela lei do cosseno
    angleToTarget = cosLaw(x1,y1,x2,y2,target(1),target(2));
    angleToNextTarget = cosLaw(x1,y1,x2,y2,nextTarget(1),nextTarget(2));
    
    % Verificacao esq ou dir
    if (x2-x1) == 0
        % o carro esta olhando para cima     
         if target(1) > x2
             %esta a dir
            angleToTarget = angleToTarget * (-1);
         end
         if nextTarget(1) > x2
             
            angleToNextTarget = angleToNextTarget * (-1);
         end
    else
        %calcula a eq da reta
         m = (y2-y1)/(x2-x1);
         b = y1 - m*x1;
         
         %verifica o checkpoint 
         y = m*target(1) + b;         
         if m > 0 && target(2) < y
            angleToTarget = angleToTarget * (-1);
         elseif m < 0 && target(2) > y
            angleToTarget = angleToTarget * (-1);
         end
         
         %verifica o prox checkpoint 
         y = m*nextTarget(1) + b;
         if m > 0 && nextTarget(2) < y
            angleToNextTarget = angleToNextTarget * (-1);
         elseif m < 0 && nextTarget(2) > y
            angleToNextTarget = angleToNextTarget * (-1);
         end
    end
    
    %Se o carro estiver olhando para a esq, o sentido muda
    if y2 < y1
        angleToNextTarget = angleToNextTarget * (-1);
        angleToTarget = angleToTarget * (-1);
    end
    
    % inferencia fuzzy
    resultadoFIS = evalfis([d/carMaxSpeed currentSpeed/carMaxSpeed angleToTarget angleToNextTarget],myfuzzy);
    
    %controle de aceleracao
    speedResult = resultadoFIS(1) * carMaxSpeed;
    if speedResult+acceleration > currentSpeed
        currentSpeed = currentSpeed+acceleration;
    elseif speedResult-acceleration < currentSpeed
        currentSpeed = currentSpeed-acceleration;
    else
        currentSpeed = speedResult;
    end

    %controle da direcao. Pode ser direto pois o controlador limita
    currentSteer = resultadoFIS(2);
    if (currentSteer > 0.5)
        currentSteer = 0.5;
    elseif (currentSteer < -0.5)
        currentSteer = -0.5;
    end
    
    %atualiza o carro
    posicaoNova = simuCar(posicaoAtual,currentSteer,currentSpeed);
    
    % desenho
    carTracker(i,:) = posicaoNova;
end

cost = dCost^3 / triggeredPoints;

end

