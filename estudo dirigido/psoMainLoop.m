function [ pos,localBestPos,gBest,gBestPos,gIndex,fitness,localFitness,velocity ] = psoMainLoop( n, d,c1,c2,w,pos,localBestPos,gBest,gBestPos, fitness,localFitness, velocity, costFunction, p,myfuzzy,track)
%PSOMAINLOOP Summary of this function goes here
%   Detailed explanation goes here
    
% Evaluate new positions
    for i= 1: n
        fitness(i) = costFunction(pos(:,i),p,myfuzzy,track);   
    end
    
    % Get the best local position 
    for i = 1 : n
        if fitness(i) < localFitness(i)
           localFitness(i)  = fitness(i);  
           localBestPos(:,i) = pos(:,i);
        end   
    end
    
    % Find current minimum fitness (gBest) on gIndex
    [currentGBest,gIndex] = min(localFitness) ;
    
    % Get Position of gBest if it is better
    if currentGBest < gBest
        gBest = currentGBest;
        for i= 1: n
            gBestPos(:,i) = pos(:,gIndex);
        end
    end
    
    R1 = rand(d,n);
    R2 = rand(d,n);
    
    % Analize velocity
    velocity = w *velocity + c1*(R1.*(localBestPos-pos)) + c2*(R2.*(gBestPos-pos));

    % Walk the particles
    for i=1:n
        for j=1:d
            target = pos(j,i) + velocity(j,i);
            if (target >= 0)
                if  (target <= 1)
                    pos(j,i) = target;
                else
                    pos(j,i) = 1;
                end
            else
                pos(j,i) = 0;
            end
        end
    end

end

