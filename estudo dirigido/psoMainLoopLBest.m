function [ pos,localBestPos,gBest,gBestPos,gIndex,fitness,localFitness,velocity ] = psoMainLoop( n, d,c1,c2,w,pos,localBestPos,gBest,gBestPos, fitness,localFitness, velocity, costFunction, p,myfuzzy,track)
%PSOMAINLOOP Summary of this function goes here
%   Detailed explanation goes here
    
% Evaluate new positions
    for i= 1: n
        fitness(i) = costFunction(pos(:,i),p,myfuzzy,track);   
    end
    
    % Get the best local position 
    for i = 1 : n
        if fitness(i) < localFitness(i)
           localFitness(i)  = fitness(i);  
           localBestPos(:,i) = pos(:,i);
        end   
    end
    
    % Find current minimum fitness (gBest) on gIndex
    [currentGBest,gIndex] = min(localFitness) ;
    if currentGBest < gBest
        gBest = currentGBest;
    end

    % Get Position of gBest if it is better
    for i= 1: n
	aux = 1:3;
	if i==1
		a1 = n;
	else
		a1 = i-1;
	end
	if i==n
		a3 = 1;
	else
		a3 = i+1;
	end
	aux(1) = localFitness(a1);
	aux(2) = localFitness(i);
	aux(1) = localFitness(a3);

	[lixo,auxI] = sort(aux);

	if auxI(1) == 1
		lIndex = a1;
	elseif auxI(1) == 2
		lIndex = i;
	else
		lIndex = a3;
	end
       gBestPos(:,i) = pos(:,lIndex);
    end
    
    R1 = rand(d,n);
    R2 = rand(d,n);
    
    % Analize velocity
    velocity = w *velocity + c1*(R1.*(localBestPos-pos)) + c2*(R2.*(gBestPos-pos));

    % Walk the particles
    for i=1:n
        for j=1:d
            target = pos(j,i) + velocity(j,i);
            if (target >= 0)
                if  (target <= 1)
                    pos(j,i) = target;
                else
                    pos(j,i) = 1;
                end
            else
                pos(j,i) = 0;
            end
        end
    end

end

