clear ; close all; clc; pause on;

% inicializa
track = dlmread('track2.txt');
%track = zeros(20,2);
%for i= 1:20
%   track(i,1) = i;
%   track(i,2) = sin(i);
%end
escala = 100;

track = track * escala;

trackSize = size(track,1);

tempo = escala*10;

carTracker = zeros(tempo,3);
otherTrackingStuff = zeros(tempo,3);

checkpoint = 1;
currentSpeed = 10;
currentSteer = 0;

myfuzzy = readfis('treinado19.fis');
carTracker(1,1) = 1.4;
for i = 2:tempo
   
    posicaoAtual = carTracker(i-1,:);    
    target = track(checkpoint,:);
    
    % se o checkpoint é o ultimo ponto, o proximo é o primeiro da lista
    if checkpoint+1 > trackSize
        nextTarget = track(1,:);
    else
        nextTarget = track(checkpoint+1,:);
    end
    
    % calcula distancia euclidiana ate o checkpoint
    X = [posicaoAtual(2),posicaoAtual(3);target(1),target(2)];
    distance = pdist(X,'euclidean');
    
    % se estiver perto suficiente, consideramos que atravessou o checkpoint
    if distance <= 15
       checkpoint = checkpoint +1;
       if (checkpoint > trackSize)
           checkpoint = 1;
           break;
       end
    end    
    
    % pontos para a reta que define esq ou dir
    x1 = posicaoAtual(2);
    y1 = posicaoAtual(3);
    x2 = posicaoAtual(2) + cos(posicaoAtual(1));
    y2 = posicaoAtual(3) + sin(posicaoAtual(1));
    
    %calcula angulos pela lei do cosseno
    angleToTarget = cosLaw(x1,y1,x2,y2,target(1),target(2));
    angleToNextTarget = cosLaw(x1,y1,x2,y2,nextTarget(1),nextTarget(2));
    
    % Verificacao esq ou dir
    if (x2-x1) == 0
        % o carro esta olhando para cima     
         if target(1) > x2
             %esta a dir
            angleToTarget = angleToTarget * (-1);
         end
         if nextTarget(1) > x2
             
            angleToNextTarget = angleToNextTarget * (-1);
         end
    else
        %calcula a eq da reta
         m = (y2-y1)/(x2-x1);
         b = y1 - m*x1;
         
         %verifica o checkpoint 
         y = m*target(1) + b;         
         if m > 0 && target(2) < y
            angleToTarget = angleToTarget * (-1);
         elseif m < 0 && target(2) > y
            angleToTarget = angleToTarget * (-1);
         end
         
         %verifica o prox checkpoint 
         y = m*nextTarget(1) + b;
         if m > 0 && nextTarget(2) < y
            angleToNextTarget = angleToNextTarget * (-1);
         elseif m < 0 && nextTarget(2) > y
            angleToNextTarget = angleToNextTarget * (-1);
         end
    end
    
    %Se o carro estiver olhando para a esq, o sentido muda
    if y2 < y1
        angleToNextTarget = angleToNextTarget * (-1);
        angleToTarget = angleToTarget * (-1);
    end
    
    % inferencia fuzzy
    resultadoFIS = evalfis([distance currentSpeed angleToTarget angleToNextTarget],myfuzzy);
    
    %controle de aceleracao
    acceleration = 1;
    speedR = resultadoFIS(1) * 20;
    if speedR+acceleration > currentSpeed
        currentSpeed = currentSpeed+acceleration;
    elseif speedR-acceleration < currentSpeed
        currentSpeed = currentSpeed-acceleration;
    else
        currentSpeed = speedR;
    end

    %controle da direcao. Pode ser direto pois o controlador limita
    currentSteer = resultadoFIS(2);
    if (currentSteer > 0.5)
        currentSteer = 0.5;
    elseif (currentSteer < -0.5)
        currentSteer = -0.5;
    end
    
    %atualiza o carro
    posicaoNova = simuCar(posicaoAtual,currentSteer,currentSpeed);
    
    % desenho
    carTracker(i,:) = posicaoNova;
    otherTrackingStuff(i,1) = currentSteer;
    otherTrackingStuff(i,2) = angleToTarget;
    otherTrackingStuff(i,3) = angleToNextTarget;
    
    fprintf('desiredSteer: %f desiredSpeed: %f distance:%f \nspeed:%f angleTarget: %f angleNext: %f',resultadoFIS(2),resultadoFIS(1)*20,distance,currentSpeed,angleToTarget,angleToNextTarget);
    display(posicaoNova);    
   carAtual = carTracker(1:i,:);
   %subplot(1,2,1);
   plot(track(:,1),track(:,2),'b-o',carAtual(:,2),carAtual(:,3),'r-',target(1),target(2),'ro',nextTarget(1),nextTarget(2),'go');
   legend('track','car','start','end');
   
   %subplot(1,2,2);
   %x = 1:i;
   %plot(x,otherTrackingStuff(x,1),'b',x,otherTrackingStuff(x,2),'g',x,otherTrackingStuff(x,3),'r');
   
   pause(0.1); 
   clc;
end