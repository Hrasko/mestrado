function [ fuzzyDecoded ] = decodeFuzzy( pos, fuzzyBefore, p )
%DECODEFCP Summary of this function goes here
%   Detailed explanation goes here
fuzzyDecoded = fuzzyBefore;

% 1 12
inp1 = pos;
inp1(13:31) = [];
inp1 = inp1*100;
inp1 = sort(inp1);

% 13 24
inp2 = pos;
inp2(25:31) = [];
inp2(1:12) = [];
inp2 = sort(inp2);

%25 31
inp3 = pos;
inp3(1:24) = [];
inp3 = inp3*3;
inp3 = sort(inp3);

%distance to target
fuzzyDecoded.input(1).range = [0 100];
fuzzyDecoded.input(1).mf(1).params = [-1 0 inp1(1)];
fuzzyDecoded.input(1).mf(2).params = [-1 0 inp1(2) inp1(3)];
fuzzyDecoded.input(1).mf(3).params = [inp1(2) inp1(3) inp1(4) inp1(9)];
fuzzyDecoded.input(1).mf(4).params = [inp1(5) inp1(6) inp1(7) inp1(10)];
fuzzyDecoded.input(1).mf(5).params = [inp1(8) inp1(11) inp1(12) (inp1(12)+1)];

%currentspeed
fuzzyDecoded.input(2).mf(1).params = [-(inp2(12)+1) -inp2(12) -inp2(1) 0.001];
fuzzyDecoded.input(2).mf(2).params = [-inp2(12) -inp2(8) -inp2(7) -inp2(5)];
fuzzyDecoded.input(2).mf(3).params = [-0.001 0 inp2(1) inp2(2)];
fuzzyDecoded.input(2).mf(4).params = [inp2(1) inp2(2) inp2(3) inp2(4)];
fuzzyDecoded.input(2).mf(5).params = [inp2(3) inp2(4) inp2(5) inp2(6)];
fuzzyDecoded.input(2).mf(6).params = [inp2(5) inp2(7) inp2(8) inp2(10)];
fuzzyDecoded.input(2).mf(6).params = [inp2(9) inp2(11) inp2(12) (inp2(12)+1)];

%angle to target
fuzzyDecoded.input(3).mf(1).params = [-4 -3 -inp3(7)];
fuzzyDecoded.input(3).mf(2).params = [-3 -inp3(7) -inp3(6) -inp3(5)];
fuzzyDecoded.input(3).mf(3).params = [-inp3(6) -inp3(4) -inp3(2) -inp3(1)];
fuzzyDecoded.input(3).mf(4).params = [-4 -3 -inp3(2) 0];
fuzzyDecoded.input(3).mf(5).params = [-inp3(3) 0 inp3(3)];
fuzzyDecoded.input(3).mf(6).params = [0 inp3(3) 3 4];
fuzzyDecoded.input(3).mf(7).params = [inp3(1) inp3(2) inp3(4) inp3(6)];
fuzzyDecoded.input(3).mf(8).params = [inp3(5) inp3(6) inp3(7) 3];
fuzzyDecoded.input(3).mf(9).params = [inp3(7) 3 4];

fuzzyDecoded.input(4) = fuzzyDecoded.input(3);
fuzzyDecoded.input(4).name = 'angleToTarget';

fuzzyDecoded.output(1) = fuzzyDecoded.input(2);
fuzzyDecoded.output(1).name = 'desiredSpeed';

end