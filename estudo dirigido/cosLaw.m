function [ theta ] = cosLaw( x1,y1,x2,y2,x3,y3 )
%COSLAW Summary of this function goes here
%   Detailed explanation goes here

%p1,p2 -> edge points
%c -> vertex point

p12 = segmentLenght(x1,y1,x2,y2);
p13 = segmentLenght(x1,y1,x3,y3);
p23 = segmentLenght(x2,y2,x3,y3);

theta = acos((p12^2 + p13^2 - p23^2)/(2*p12*p13));

end