function [ pos,localBestPos,gBest,gBestPos,fitness,localFitness,velocity ] = psoInit( n,steps,d,c1,c2,momentum,costFunction,p,myfuzzy,track )
%PSOINIT Summary of this function goes here
%   Detailed explanation goes here

%fitness indicates how well is the particule doing.
fitness = ones(n,1);


% randomize positions
pos = rand(d,n);
localBestPos = pos;

% randomize initial velocity. 
velocity = momentum*rand(d,n);

% evaluate initial positions
for i= 1: n

   fitness(i) = costFunction(pos(:,i),p,myfuzzy,track);
   
end

% Initialy, local fitness is current fitness
localFitness = fitness;

[gBest,gIndex] = min(localFitness) ;

    % Get Position of gBest if it is better
    for i= 1: n
        aux = 1:3;
        if i==1
            a1 = n;
        else
            a1 = i-1;
        end
        if i==n
            a3 = 1;
        else
            a3 = i+1;
        end
        aux(1) = localFitness(a1);
        aux(2) = localFitness(i);
        aux(1) = localFitness(a3);

        [lixo,auxI] = sort(aux);

        if auxI(1) == 1
            lIndex = a1;
        elseif auxI(1) == 2
            lIndex = i;
        else
            lIndex = a3;
        end
        
       gBestPos(:,i) = pos(:,lIndex);
    end
    
% Walk the particles
for i=1:n
    for j=1:d
        target = pos(j,i) + velocity(j,i);
        if (target >= 0 && target <= 1)
            pos(j,i) = target;
        end
    end
end

end

