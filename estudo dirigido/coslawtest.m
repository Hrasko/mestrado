clear ; close all; clc; pause on;

samples = rand(100,2) * 2;

left = zeros(100,2);
li = 1;
right = zeros(100,2);
lr = 1;

posicaoAtual = rand(1,3);


x1 = posicaoAtual(2);
y1 = posicaoAtual(3);
x2 = posicaoAtual(2) + cos(posicaoAtual(1));
y2 = posicaoAtual(3) - sin(posicaoAtual(1));

for i=1:100
    
    x3 = samples(i,1);
    y3 = samples(i,2);
    
    theta = cosLaw( x1,y1,x2,y2,x3,y3 );
    
    if (x2-x1) == 0 && x3 > x2
     theta = theta * (-1);
    else
         m = (y2-y1)/(x2-x1);
         b = y1 - m*x1;
         y = m*x3 + b;
         if m > 0 && y3 < y
            theta = theta * (-1);
         elseif m < 0 && y3 > y
            theta = theta * (-1);
         end
         
         
    end
    if y2 < y1
        theta = theta * (-1);
    end
   if theta > 0
       left(li,:) = samples(i,:);
       li = li+1;
   else
       right(lr,:) = samples(i,:);
       lr = lr+1;
   end
end

plot(left(1:li,1),left(1:li,2),'b+',right(1:lr,1),right(1:lr,2),'g+',x1,y1,'ro',x2,y2,'r*');