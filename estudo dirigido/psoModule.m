function [ best,bestFit,interactions,BestCounter,bestCar ] = psoModule( n,steps,d,c1,c2,w,costFunction,p,myfuzzy,track)



% restriction -> matrix with limits
%p -> pause between each interaction

%% Initialization
clc;

[ pos,localBestPos,gBest,gBestPos,fitness,localFitness,velocity ] = psoInit( n,steps,d,c1,c2,w,costFunction,p,myfuzzy,track );

%% Drawing initialization
%xv = [-interval:0.1:interval];
%yv = [-interval:0.1:interval];
%[u,v] = meshgrid(xv,yv);
%s = funcao(u,v);
%nn = 1:n;
%gBestTracker = 0*ones(n);

%% Main Loop
interactions = 0;
BestCounter = 0;
best = Inf;
while (gBest > 0.001 && interactions < steps)    
    interactions = interactions + 1;
    
    [ pos,localBestPos,gBest,gBestPos,gIndex,fitness,localFitness,velocity ] = psoMainLoop( n,d,c1,c2,w, pos,localBestPos,gBest,gBestPos, fitness,localFitness, velocity, costFunction, p, myfuzzy,track );
    
    if (gBest < best)
        bestFit = gBest;
        best = gBestPos;
        BestCounter = interactions;
        [lixo,bestCar] = costFunction(best,p, myfuzzy,track);
        myfuzzy2 = decodeFuzzy(pos,myfuzzy);
        %writefis(myfuzzy2,'treinado');
    end
    
    clc;
    fprintf('iteracao: %d best encontrado em: %d \n',interactions,BestCounter);
    plot(track(:,1),track(:,2),'b-o',bestCar(:,2),bestCar(:,3),'r-');
    legend('track','car');
end 