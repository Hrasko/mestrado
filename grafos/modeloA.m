mapa = dlmread('mapa.txt');
indices = dlmread('indices.txt');
indicesR = dlmread('indicesR.txt');
grafo = dlmread('modeloa.txt');
paredes = dlmread('paredesa.txt');
dparedes = dlmread('dparedesa.txt');
n = size(mapa,1);


n2 = n*n;
OPEN=zeros(n2,1);
CLOSED=zeros(n2,1);
PARENT=zeros(n2,1);
F=zeros(n2,1);
G=zeros(n2,1);
H=zeros(n2,1);
k=0;
for i=1:n
    for j=1:n
        if(paredes(i,j) > 0)
            CLOSED(indices(i,j))=1; 
            k=k+1;
        end
    end
end

CLOSED_COUNT=k/2;

node = indices(5,5);
target = indices(1,1);
OPEN(node) = 1;
OPEN_COUNT = 1;
while(sum(OPEN) > 0)
    reachable = vizinhos2(indicesR(node,1),indicesR(node,2),n,indices,paredes);
    for i=1:4
        if (reachable(i) > 0 && CLOSED(reachable(i)) < 1)
            if OPEN(reachable(i)) > 0
                g = 1 + G(node);
                if G(reachable(i)) > g
                    PARENT(reachable(i)) = node;
                    G(reachable(i)) = g;
                    F(reachable(i)) = G(reachable(i))+H(reachable(i));
                end
            else
                OPEN(reachable(i)) = 1;
                PARENT(reachable(i)) = node;
                G(reachable(i)) = 1 + G(node);
                H(reachable(i)) = manhattamDistance( reachable(i),target,indicesR );
                F(reachable(i)) = G(reachable(i))+H(reachable(i));
            end
        end
    end
    OPEN(node)=0;
    CLOSED(node)=1;
    OPEN_COUNT = OPEN_COUNT-1;
    CLOSED_COUNT = CLOSED_COUNT+1;

    [lixo,sorted] = sort(F);
    for i=1:n2
       if F(sorted(i)) >0 && OPEN(sorted(i)) > 0 && CLOSED(sorted(i)) < 1
          node = sorted(i);
          break;
       end
    end
end
node = indices(5,5);
path = [];
dummy = 1;
path(dummy)=target;
while(node~=target)
   dummy = dummy+1;
   target = PARENT(target);
   path(dummy)=target;
end

display(path);


