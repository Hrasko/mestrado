mapa = dlmread('mapa.txt');
indices = dlmread('indices.txt');
indicesR = dlmread('indicesR.txt');
grafo = dlmread('modeloa.txt');
paredes = dlmread('paredesa.txt');
dparedes = dlmread('dparedesa.txt');

n = size(mapa,1);
robot = indices(7,7);
target = indices(2,1);
path = aStar(n,indices,indicesR,paredes,robot,target);

while robot ~= target
    clf;
    plot(indicesR(target,1),indicesR(target,1),'ro');
    axis([1 n+1 1 n+1])
    %grid on;
    hold on;
    
    for i=1:n
       for j=1:n
          if dparedes(i,j) > 0 
              plot(i,j,'rx');
          end
       end
    end
    
    for i=1:n
       for j=1:n
          if paredes(i,j) > 0 
              plot(i,j,'bx');
          end
       end
    end
    
    

    caminho = zeros(size(path,2),2);

    for i=1:size(path,2)
        caminho(i,1) = indicesR(path(i),1);
        caminho(i,2) = indicesR(path(i),2);
        %plot(caminho(i,1),caminho(i,2),'bo');
        
        if path(i) == robot
            plot(caminho(i,1),caminho(i,2),'gx');
            if i > 1
                robot = path(i-1);
                vizinhos = vizinhos2(indicesR(robot,1),indicesR(robot,2),n,indices,paredes);
                display(vizinhos);
                somethingChanged = 0;
                for j=1:4
                    if vizinhos(j) > 0
                        x = indicesR(vizinhos(j),1);
                        y = indicesR(vizinhos(j),2);
                        if paredes(x,y) ~= dparedes(x,y)
                            paredes(x,y) = dparedes(x,y);
                            somethingChanged = 1;
                            display(somethingChanged);
                        end
                    end
                end
                if (somethingChanged > 0) 
                    display(path);
                    path = aStar(n,indices,indicesR,paredes,robot,target);
                    display(path);
                    break;
                end
            end
            pause(0.5);
        end
        
    end

    plot(caminho(:,1),caminho(:,2),'b-');
end