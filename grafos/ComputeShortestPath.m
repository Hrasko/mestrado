function [ RHS,U,LL,LLi,PARENT ] = ComputeShortestPath( U_,start,g,rhs,km,n,indices,indicesR,paredes,parent,ll,lli,infinito )
%COMPUTESHORTESTPATH Summary of this function goes here
%   Detailed explanation goes here
U = U_;
RHS = rhs;
LL = ll;
LLi = lli;
PARENT = parent;
[lixo,sorted] = sortrows(U);
topKey = [U(sorted(1),1) U(sorted(1),2)];
cStart = CalcKey( start,start,g,rhs,km,indicesR );
uIndex = sorted(1);
while (CompareTuple(topKey,cStart) < 0 || rhs(start) ~= g(start))
    kOld = topKey;
    LL.removeFirstOccurrence(uIndex);
    uTuple = [0 0];
    [uTuple(1) uTuple(2)] =CalcKey( uIndex,start,g,rhs,km,indicesR );
    if (CompareTuple(kOld,uTuple) < 0)
        [U(sorted(1),1) U(sorted(1),2)] = CalcKey( uIndex,start,g,rhs,km,indicesR );
    else
        
        if g(uIndex) > rhs(uIndex)
            g(uIndex) = rhs(uIndex);
        else
            g(uIndex) = infinito;
            [RHS,U,LL,LLi] = UpdateVertex( uIndex,start,PARENT,U,g,rhs,km,indicesR,paredes,LL,LLi,infinito );
        end
        
        reachable = vizinhos2(indicesR(uIndex,1),indicesR(uIndex,2),n,indices,paredes);
        for i = 1:4
           if reachable(i) > 0
               PARENT(reachable(i)) = uIndex;
               [RHS,U,LL,LLi] = UpdateVertex( reachable(i),start,PARENT,U,g,rhs,km,indicesR,paredes,LL,LLi,infinito );
           end
        end
    end
    
    [lixo,sorted] = sortrows(U);
    topKey = [U(sorted(1),1) U(sorted(1),2)];
    
end
end

