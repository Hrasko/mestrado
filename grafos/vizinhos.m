function [ output ] = vizinhos( i,j,n,indices,grafo )
%VIZINHOS Summary of this function goes here
%   Detailed explanation goes here

output = zeros(4,1);
b = 0;
x = indices(i,j);
for a=1:n
    if (a ~= j && grafo(indices(i,a),x) < 1000)
        b = b+1;
        output(b) = indices(i,a);
    end
    if (a ~= i && grafo(indices(a,j),x) < 1000)
        b = b+1;
        output(b) = indices(a,j);
    end
end

end