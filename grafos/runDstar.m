mapa = dlmread('mapa.txt');
indices = dlmread('indices.txt');
indicesR = dlmread('indicesR.txt');
grafo = dlmread('modeloa.txt');
paredes = dlmread('paredesa.txt');
dparedes = dlmread('dparedesa.txt');

n = size(mapa,1);
robot = indices(7,7);
target = indices(2,1);
start = robot;
n2 = n*n;
infinito = n2*n2;
U = ones(n2,2)*infinito; %tuplas
PARENT=zeros(n2,1); %indica se � um no fronteira
km = 0;
RHS = ones(n2,1)*infinito;
G = ones(n2,1)*infinito;
RHS(target) = 0;
[U(target,1),U(target,2)] = CalcKey( target,start,G,RHS,km,indicesR );
ll=java.util.LinkedList;
lli=ll.listIterator;
lli.add(target);

[ RHS,U,LL,LLi,PARENT ] = ComputeShortestPath( U,start,G,RHS,km,n,indices,indicesR,paredes,PARENT,ll,lli,infinito );