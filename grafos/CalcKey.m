function [ t1,t2 ] = CalcKey( node,start,g,rhs,km,indicesR )
%CALCKEY Summary of this function goes here
%   Detailed explanation goes here
t2 = min(g(node),rhs(node));
t1 = t2 + manhattamDistance( start,node,indicesR ) + km;

end

