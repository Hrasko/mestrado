function [ RHS,U,LL,LLi ] = UpdateVertex( uIndex,start,parent,U_,g,rhs,km,indicesR,dparedes,ll,lli,infinito )
%UPDATEVERTEX Summary of this function goes here
%   Detailed explanation goes here
tic;
a=java.util.LinkedList;
li=a.listIterator;
RHS = rhs;
U = U_;
LL = ll;
LLi = lli;
for i=1:size(parent,2)
    if parent(i) == uIndex
        li.add(parent(i));
    end
end

if uIndex ~= start
    min = infinito;
    for i=0:size(a)-1
       s=a.get(i);
       p = dparedes(indicesR(s,1),indicesR(s,2)); %existe obstaculo em s?
       if p < 1
           m = g(s);
           if (min < m)
              min = m;
           end
       end
    end
    RHS(uIndex) = min;
end

if ll.contains(uIndex) > 0
    ll.a.removeFirstOccurrence(uIndex);
end

if g(uIndex) ~= RHS(uIndex)
   lli.add(uIndex);
   [U(uIndex,1),U(uIndex,2)] = CalcKey( uIndex,start,g,RHS,km,indicesR );
end
clc;
x = toc;
display(x*10000);
end

