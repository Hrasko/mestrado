function [ output ] = vizinhos2( i,j,n,indices,paredes )
%VIZINHOS Summary of this function goes here
%   Detailed explanation goes here

output = zeros(4,1);
b = 0;
for a=1:n
    if (abs(a-j) ==1 && paredes(i,a) < 1)
        b = b+1;
        output(b) = indices(i,a);
    end
    if (abs(a-i) ==1 && paredes(a,j) < 1)
        b = b+1;
        output(b) = indices(a,j);
    end
end

end