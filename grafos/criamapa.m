n = 10;
m = [10 9 8 7 6 5 4 3 2 1];
mapa = [m; m; m; m; m; m; m; m; m; m];
indices = mapa;
indicesR = zeros(n*n,2);
paredes = zeros(n,n);
grafo = ones(n*n,n*n)*(n*n*n);
c = 0;
for i=1:n
   for j=1:n
       c = c+1;
       indices(i,j) = c;
       indicesR(c,1) = i;
       indicesR(c,2) = j;
   end
end

for i=4:7
    paredes(3,i) = 1;%mapa(3,i);
end

for i=3:7
    paredes(i,4) = 1;%mapa(i,4);
end

for i=1:n
   for j=1:n
       if (i ~= j)       
           if (i>1)
               if (paredes(i-1,j) < 1)
                grafo(indices(i-1,j),indices(i,j)) = 1;
               end
           end
           if (i<n)
               if (paredes(i+1,j) < 1)
                grafo(indices(i+1,j),indices(i,j)) = 1;
               end
           end
           if (j>1)
               if (paredes(i,j-1) < 1)
                grafo(indices(i,j-1),indices(i,j)) = 1;
               end
           end
           if (j<n)
               if (paredes(i,j+1) < 1)
                grafo(indices(i,j+1),indices(i,j)) = 1;
               end
           end
       end
   end
end
dlmwrite('mapa.txt',mapa);
dlmwrite('indices.txt',indices);
dlmwrite('indicesR.txt',indicesR);
dlmwrite('modeloa.txt',grafo);
dlmwrite('paredesa.txt',paredes);
plot(mapa,'bo');
hold on;
plot(mapa.*paredes,'rx');