function [ cost ] = t1cost( pos, varargin)
%T1COST Summary of this function goes here
%   Detailed explanation goes here
v = varargin{1};

training = v{1};
tSize = v{2};
nnConfig = v{3};
theta = decodeTheta(pos, nnConfig);
%theta1 = theta(1:2,:,1);
%theta2 = theta(:,1:1,2);
yp = 1:tSize;
cost = 0;

for i=1:tSize
    yp(i) = nn (training(i,1),theta,nnConfig);
    cost = ((yp(i) - training(i,2)) .^ 2) + cost;
end


cost = cost ./ tSize;

end