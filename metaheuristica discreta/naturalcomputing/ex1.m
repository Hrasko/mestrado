clear ; close all; clc


Xfile = dlmread('X_File.txt');
Yfile = dlmread('Y_File.txt');

restrictions = [1 5;0.01 1;0 50];

[gBest,bestPos] = pso([50 50 3 1.49445 1.49445 0.729],restrictions,@ex1Cost,0.1,@drawEx1,Xfile,Yfile)