function [y]=fitness(x,K,G,D)
    y=K/(1+exp(-G*(x-D)));
end