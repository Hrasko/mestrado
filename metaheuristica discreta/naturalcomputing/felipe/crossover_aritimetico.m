% Crossover aritimetico
% Geracao de dois novos individuos atraves do cruzamento crossover aritimetico
% Dois individuos e um beta(0,1) sao selecionados aleatoriamente
% c1 = beta*p1 + (1-beta)*p2
% c2 = (1-beta)*p1 + beta*p2
% Px=POPULACAO
% [c1,c2] NOVOS INDIVIDUOS

function [out]=crossover_aritimetico(Pk,Pg,Pd,individuo1,individuo2)  
    beta = rand();
    
    p1k=Pk(individuo1);
    p2k=Pk(individuo2);
    c1k=(beta*p1k)+((1-beta)*p2k);
    c2k=((1-beta)*p1k)+(beta*p2k);      
    Pk(individuo1)=c1k;
    Pk(individuo2)=c2k;

    p1g=Pg(individuo1);
    p2g=Pg(individuo2);
    c1g=(beta*p1g)+((1-beta)*p2g);
    c2g=((1-beta)*p1g)+(beta*p2g);
    Pg(individuo1)=c1g;
    Pg(individuo2)=c2g;

    p1d=Pd(individuo1);
    p2d=Pd(individuo2);
    c1d=(beta*p1d)+((1-beta)*p2d);
    c2d=((1-beta)*p1d)+(beta*p2d);
    Pd(individuo1)=c1d;
    Pd(individuo2)=c2d;

    out=[Pk,Pg,Pd];
end