function out=ga()
   clc;
   clf;
   
   % NUMERO DE GERACOES
   generation=100;
   
   % NUMERO DE INDIVIDUOS
   psize=40;
   
   % PROBABILIDADE DE CROSSOVER
   pc=.6;
   
   % PROBABILIDADE DE MUTACAO
   pm=.1;
   
   % INICIALIZACAO DA POPULACAO
   Pk=zeros(psize,1);
   Pg=zeros(psize,1);
   Pd=zeros(psize,1);
   
   inputX=dlmread('X_File.txt');
   inputY=dlmread('Y_File.txt');
   
   bestf=0; % MELHOR FITNESS
   bestp=0; % MELHOR INDIVIDUO
   bestg=0; % GERACAO DO MELHOR INDIVIDUO
   
   % MINIMOS E MAXIMOS DOS PARAMETROS
   kmin=1;
   kmax=5;
   gmin=.01;
   gmax=1;
   dmin=0;
   dmax=50;
   
   % INICIALIZANDO A POPULACAO
   for i=1:psize
       Pk(i)=kmin+(rand*(kmax-kmin)); % K
       Pg(i)=gmin+(rand*(gmax-gmin)); % G
       Pd(i)=dmin+(rand*(dmax-dmin)); % D
       
       % CALCULANDO O FITNESS
       somatorio=0;
       for j=1:length(inputX)
           k=Pk(i);
           g=Pg(i);
           d=Pd(i);
           x=inputX(j);
           y=inputY(j);
           ye=fitness(x,k,g,d);
           e=(y-ye)^2;
           somatorio=somatorio+e;
       end
       F(i)=somatorio;
   end    
   
   % LOCALIZANDO O MELHOR INDIVIDUO
   bestf=F(1);
   bestp=1;
   for i=2:psize
       if bestf>F(i)
           bestf=F(i);
           bestp=i;
       end
   end
   bestg=0;
   
   % GERANDO OS PONTOS DO MELHOR INDIVIDUO
   for i=1:length(inputX)
       k=Pk(bestp);
       g=Pg(bestp);
       d=Pd(bestp);
       x=inputX(i);
       Pye(i)=fitness(x,k,g,d);
   end  
    
   subplot(2,1,2);
   plot(inputX,inputY,'r.',inputX,Pye,'b.')
   grid on
   hold off
   legend('data','estimative')
   
   for i=1:generation
       % SELECAO
       P=selecao_torneio(Pk,Pg,Pd,F,inputX,inputY);
       Pks=P(:,1);
       Pgs=P(:,2);
       Pds=P(:,3);
       
       % CROSSOVER
       Pkc=Pks;
       Pgc=Pgs;
       Pdc=Pds;
       for m=1:ceil(psize*pc)
           individuo1=natural_rand(1,psize);
           individuo2=natural_rand(1,psize);
           c=crossover_aritimetico(Pks,Pgs,Pds,individuo1,individuo2);
           Pkc=c(:,1);
           Pgc=c(:,2);
           Pdc=c(:,3);
       end
       
       % MUTACAO
       Pkm=Pkc;
       Pgm=Pgc;
       Pdm=Pdc;
       for m=1:ceil(psize*pm)
           individuo=natural_rand(1,psize);
           Pkm(individuo)=mutacao_gaussiana(Pkm,individuo);
           Pgm(individuo)=mutacao_gaussiana(Pgm,individuo);
           Pdm(individuo)=mutacao_gaussiana(Pdm,individuo);
       end

       Pk=Pkm;
       Pg=Pgm;
       Pd=Pdm;
       % CALCULANDO O FITNESS
       for m=1:psize
           somatorio=0;
           for j=1:length(inputX)
               k=Pk(m);
               g=Pg(m);
               d=Pd(m);
               x=inputX(j);
               y=inputY(j);
               ye=fitness(x,k,g,d);
               e=(y-ye)^2;
               somatorio=somatorio+e;
           end
           F(m)=somatorio;
       end
       
       % LOCALIZANDO O MELHOR INDIVIDUO
       for j=1:psize
           if bestf>F(j)
               bestf=F(j);
               bestp=j;
               bestg=i;
           end
       end
        
       Fbest(i)=bestf;
       subplot(3,2,1);
       plot([1:i],Fbest,'b-')
       xlabel('generation')
       ylabel('fitness')
       grid on
       hold on
       drawnow
       
       % GERANDO OS PONTOS DO MELHOR INDIVIDUO
       for m=1:length(inputX)
           k=Pk(bestp);
           g=Pg(bestp);
           d=Pd(bestp);
           x=inputX(m);
           Pye(m)=fitness(x,k,g,d);
       end
       subplot(3,2,2);
       plot(inputX,inputY,'r.',inputX,Pye,'b.')
       grid on
       hold off
       legend('data','estimative');
       subplot(3,2,4);
       scatter3(Pk,Pg,Pd,'h');
       axis([1 5 0.01 1 0 50]);
   end
   
   out=[Pk(bestp) Pg(bestp) Pk(bestp) bestf];
end