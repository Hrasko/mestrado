function [ r ] = ackley( x,y )
%ACKLEY Summary of this function goes here
%   Detailed explanation goes here
    r = -20.*exp(-0.2 .*sqrt(0.5 .*(x.^2+y.^2)))-exp(0.5.*(cos(2.*pi.*x)+(cos(2.*pi.*y))))+20+exp(1)+eps;
end