function [ cost ] = larguraBandaCost( pos, varargin)
%LARGURABANDACOST Summary of this function goes here
%   Detailed explanation goes here
v = varargin{1};

A = v{1};
mSize = v{2}; 

[lixo,indices] = sort(pos);

I = eye(mSize,mSize);

P = I(indices,:);

R = P*A*P';

[i,j] = find(R);
cost = max(i-j);

end

