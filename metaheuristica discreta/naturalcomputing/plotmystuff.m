clear
clc

xv = [-5:0.1:5];
yv = [-5:0.1:5];
[u,v] = meshgrid(xv,yv);
z = ackley(u,v);
%tx = size(u);
%ty = size(v);
%z = u;
%for a=1:tx
%    for b=1:ty
%        z(a,b) = beales(xv(a),yv(b));
%    end
%end
figure;
surfc(u,v,z,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong');
colorbar