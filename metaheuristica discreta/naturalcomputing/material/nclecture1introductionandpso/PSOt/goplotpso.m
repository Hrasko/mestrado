% goplotpso.m
% simple graphing prog for use with tpso1,2,3
%
% Brian Birge
% Rev 1.0
% 1/1/3

    subplot(2,1,1)
     semilogy(1:te,tr(1:end-1)),xlabel('epoch');,ylabel('gbest');
     title(['mv=',num2str(mv),' inertia wt=',num2str(iwt(i)),', ',num2str(D),' dimensions, GbestVal= ',num2str(gbestval,10)]);            
     hold on
     semilogy(1:te,ones(size(1:te))*eg,'r-.');
     hold off
     drawnow
    subplot(2,1,2)
     plot(pos(:,1),pos(:,D),'b.')
     hold on
     plot(pbest(:,1),pbest(:,D),'y.');
     xlabel('pos dim 1');
     ylabel(['pos dim ',num2str(D)]);
     grid on
     plot(gbest(1),gbest(D),'r*');    
     hold off
     axis([-10 10 -10 10]);
     drawnow     
%    subplot(2,2,4)
%     plot(vel(:,1),vel(:,D),'b.');
%     xlabel('vel dim 1');
%     ylabel(['vel dim ',num2str(D)]);
%     grid on
%     hold on
%     plot(vel(idx1,1),vel(idx1,D),'r*');
%     hold off
%     drawnow 