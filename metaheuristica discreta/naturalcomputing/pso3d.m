function [ gBest ] = pso3d( n,steps, c1,c2, w,restriction,p,funcao)

% n-> numero de particulas
% steps -> numero de passos
% restriction -> matrix with limits
% c1, c2 -> pso parameters
% w -> pso momentum
%p -> pause between each interaction

%% Initialization
clc;

%This is tested only with three parameters
d = 3;

%fitness indicates how well is the particule doing.
fitness = ones(n,1);


% randomize positions between -interval and interval
pos = ones(d,n);
for i=1:n
    for j=1:d
        pos(j,i) = rand(1)*(restriction(j,2)-restriction(j,1))+restriction(j,1);
    end
end
localBestPos = pos;

%other init
xfile = dlmread('X_File.txt');
yfile = dlmread('Y_File.txt');

% randomize initial velocity. 
velocity = w*rand(d,n);

% evaluate initial positions
for i= 1: n
   fitness(i) = funcao(pos(1,i),pos(2,i),pos(3,i),xfile,yfile,p);
end

% Initialy, local fitness is current fitness
localFitness = fitness;

% Find minimum fitness (gBest) on gIndex
[gBest,gIndex] = min(localFitness) ;

% Get Position of gBest. IT is a vector to simplify the velocity
% calculation in the main loop
gBestPos = pos;
for i= 1: n
    gBestPos(:,i) = pos(:,gIndex);
end

% Walk the particles
for i=1:n
    for j=1:d
        target = pos(j,i) + velocity(j,i);
        if (target >= restriction(j,1) && target <= restriction(j,2))
            pos(j,i) = target;
        end
    end
end

%% Drawing initialization
%xv = [-interval:0.1:interval];
%yv = [-interval:0.1:interval];
%[u,v] = meshgrid(xv,yv);
%s = funcao(u,v);
if (p>0)
    nn = 1:n;
    gBestTracker = 0*ones(n);
    set(gcf,'Position',[66  1 1301 689]);
end
%% Main Loop

interactions = 0;
while (interactions < steps)    
    interactions = interactions + 1;
    
    % Used to track gBest for drawing purposes only
    gBestTracker(interactions) = gBest;
    
    % Evaluate new positions
    for i= 1: n
        fitness(i) = funcao(pos(1,i),pos(2,i),pos(3,i),xfile,yfile,p);   
    end
    
    % Get the best local position 
    for i = 1 : n
        if fitness(i) < localFitness(i)
           localFitness(i)  = fitness(i);  
           localBestPos(:,i) = pos(:,i);
        end   
    end
    
    % Find current minimum fitness (gBest) on gIndex
    [currentGBest,gIndex] = min(localFitness) ;

    % Get Position of gBest if it is better
    if currentGBest < gBest
        gBest = currentGBest;
        for i= 1: n
            gBestPos(:,i) = pos(:,gIndex);
        end
    end
    
    R1 = abs(randn([d n]));
    R2 = 1 - R1;
    
    % Analize velocity
    velocity = w *velocity + c1*(R1.*(localBestPos-pos)) + c2*(R2.*(gBestPos-pos));

    % Walk the particles
    for i=1:n
        for j=1:d
            target = pos(j,i) + velocity(j,i);
            if (target >= restriction(j,1))
                if  (target <= restriction(j,2))
                    pos(j,i) = target;
                else
                    pos(j,i) = restriction(j,2);
                end
            else
                pos(j,i) = restriction(j,1);
            end
        end
    end
    
    %% Drawing
        if (p > 0)
        x=pos(1,:);
        y=pos(2,:);
        z=pos(3,:);
        %subplot(2,2,1)
        %surfc(u,v,s,'FaceColor','interp',...
       %'EdgeColor','none',...
       %'FaceLighting','phong');
        %hold on    
        %scatter3(x,y,funcao(x,y));    
        %hold off

        subplot(4,2,1);
        plot (nn,x,'x');
        title('Valores de K');
        axis([1 n restriction(1,1) restriction(1,2)]);
        grid on

        subplot(4,2,3);
        plot (nn,y,'x');
        title('Valores de G');
        axis([1 n restriction(2,1) restriction(2,2)]);
        grid on

        subplot(4,2,5);
        plot (nn,z,'x');
        title('Valores de D');
        axis([1 n restriction(3,1) restriction(3,2)]);
        grid on

        subplot(4,2,7);
        plot (nn,localFitness,'x',nn,gBest,'ro');
        title('Fitness');
        grid on

        subplot(3,2,4);
        scatter3(x,y,z,'h');
        axis([restriction(1,1) restriction(1,2) restriction(2,1) restriction(2,2) restriction(3,1) restriction(3,2) ]);
        title('Position');

        subplot(3,2,6);
        plot(gBestTracker);
        title('gBest');

        funcao(gBestPos(1,1),gBestPos(2,1),gBestPos(3,1),xfile,yfile,p);

        %plot(nn,interactions);    
        %axis([0 n 0 n]);    

        pause(p)
    end
end 

gBestPos(:,1)