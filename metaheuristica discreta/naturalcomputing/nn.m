function [ output ] = nn( input,theta,nnConfig )
%NN Summary of this function goes here
%   Detailed explanation goes here

    %add bias
    %x = [1 input];

    %hidden layer
    %z2 = sigmoid(x*theta1);

    %add bias
    %z2b = [1 z2];

    %output layer
    %output = sigmoid(z2b*theta2);
    
    z = input;
    d = length(nnConfig)-1;
    for layer=1:d
       %add bias
       x = [1 z];
       prev = nnConfig(layer);
       next = nnConfig(layer+1);
       t = theta(1:prev+1,1:next,layer);
       output = x*t;
       z = sigmoid(output);
    end
end

