function [ theta ] = decodeTheta( v, nnConfig )
%DECODETHETA Summary of this function goes here
%   Detailed explanation goes here

    %nInput = nnConfig(1);
    %nNeurons = nnConfig(2);
    %nOutput = nnConfig(3);
    
    d = length(nnConfig)-1;
    tcount = 1;
    for k = 1:d
        prev = nnConfig(k);
        next = nnConfig(k+1);
        for i = 1:prev+1
            for j = 1:next
                theta(i,j,k) = v(tcount);
                tcount = tcount+1;
            end
        end
    end
    
    %
    %theta1 = ones(nInput+1,nNeurons);
    %tcount = 1;
    %for i=1:nInput+1
    %    for j=1:nNeurons
    %        theta1(i,j)=theta(tcount);
    %        tcount = tcount+1;
    %    end
    %end
    %theta2 = ones(nNeurons+1,nOutput);
    %for i=1:nNeurons+1
    %    for j=1:nOutput
    %        theta2(i,j)=theta(tcount);
    %        tcount = tcount+1;
    %    end
    %end    
end

