function [ gBest ] = pso( n,steps, c1,c2, w,interval,funcao)

% n-> numero de particulas
% steps -> numero de passos
% d -> dimension
% c1, c2 -> pso parameters
% w -> pso momentum

%% Initialization
clc;

%this is only tested with two dimension. Sorry :(
d = 2;

%fitness indicates how well is the particule doing.

localFitness = ones(n,1);
fitness = ones(n,1);

%random numbers
R1 = rand(d,n);
R2 = rand(d,n);

% randomize positions between -interval and interval
pos = 2*interval*(rand(d, n)-.5);
localBestPos = pos;

% randomize initial velocity. 
velocity = w*rand(d,n);

% evaluate initial positions
for i= 1: n
   fitness(i) = funcao(pos(1,i),pos(2,i));
end

% Initialy, local fitness is current fitness
localFitness = fitness;

% Find minimum fitness (gBest) on gIndex
[gBest,gIndex] = min(localFitness) ;

% Get Position of gBest. IT is a vector to simplify the velocity
% calculation in the main loop
for i= 1: n
    gBestPos(:,i) = pos(:,gIndex);
end

% Walk the particles
pos = pos+velocity;

%% Drawing initialization
xv = [-interval:0.1:interval];
yv = [-interval:0.1:interval];
[u,v] = meshgrid(xv,yv);
s = funcao(u,v);
nn = [1:n];
gBestTracker = 0*ones(n);

%% Main Loop

interactions = 0;
inertia = 0;
while (interactions < steps && inertia < 10)    
    interactions = interactions + 1;
    
    % Used to track gBest for drawing purposes only
    gBestTracker(interactions) = gBest;
    
    % Evaluate new positions
    for i= 1: n
        fitness(i) = funcao(pos(1,i),pos(2,i));        
    end
    
    % Get the best local position 
    for i = 1 : n
        if fitness(i) < localFitness(i)
           localFitness(i)  = fitness(i);  
           localBestPos(:,i) = pos(:,i);
        end   
    end
    
    % Find current minimum fitness (gBest) on gIndex
    [currentGBest,gIndex] = min(localFitness) ;

    % Get Position of gBest if it is better
    if currentGBest < gBest
        inertia = 0;
        gBest = currentGBest;
        for i= 1: n
            gBestPos(:,i) = pos(:,gIndex);
        end
    else
        if currentGBest == gBest
            inertia = inertia+1;
        end
    end
    
    R1 = abs(randn([d n]));
    R2 = 1 - R1;
    
    % Analize velocity
    velocity = w *velocity + c1*(R1.*(localBestPos-pos)) + c2*(R2.*(gBestPos-pos));

    % Walk the particles    
    pos = pos+velocity;
    
    %% Drawing
    x=pos(1,:);
    y=pos(2,:);
    subplot(2,2,1)
    surfc(u,v,s);
 %%,'FaceColor','interp',...
 %%  'EdgeColor','none',...
 %%  'FaceLighting','phong');
    hold on    
    scatter3(x,y,funcao(x,y));    
    axis([-4 4 -4 4 0 10000]);
    hold off
    
    subplot(2,2,2);
    plot (nn,localFitness,nn,gBest);
    
    subplot(2,2,3);
    plot(x, y , 'h');   
    axis([-interval interval -interval interval]);
    
    subplot(2,2,4);
    plot(gBestTracker);
    
    %plot(nn,interactions);    
    %axis([0 n 0 n]);    
    
    pause(.2)
end 

gBestPos(:,1)
