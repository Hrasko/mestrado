x = 0;
y = 100;
theta = 0;
pos = [theta x y];

time = 10000;
plotX = 1:time;
plotY = 1:time;
plotTheta = 1:time;

myfuzzy = readfis('modelfreecar.fis');

for g = 1:generations
    myfuzzy = improveMyFuzzy(myFuzzy);

    for i = 1:time
        plotTheta(i) = pos(1);
        plotX(i) = pos(2);
        plotY(i) = pos(3);

        u = evalfis([pos(3) pos(1)], myfuzzy);

        pos = simuCar(pos,u,5);


    end
end

plotT = 1:time;
subplot(3,1,1);
plot (plotT,plotX,plotT,plotY);
legend('x','y');

subplot(3,1,3);
plot(plotX,plotY)

subplot(3,1,2);
plot(plotT,plotTheta);
    
