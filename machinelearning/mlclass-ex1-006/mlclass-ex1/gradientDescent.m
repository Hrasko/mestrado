function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %


partial = zeros(2, 1);
	
	for i = 1:m
		meuY = theta(1) + theta(2) .* X(i,2);
		partial(1) = partial(1) + (meuY - y(i));
		partial(2) = partial(2) + ((meuY - y(i)) .* (X(i,2)));
	end
	
	for j = 1:2
		theta(j) = theta(j) - (alpha .* partial(j) ./ m);
	end




    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);

end

end
