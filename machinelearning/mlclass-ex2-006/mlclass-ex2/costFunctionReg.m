function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples
d = size(theta);
% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
thetaSqr = 0;
for i = 1 : m
	hTheta = theta(1);
	%thetaSqr = theta(1) .^2;
	for j=2:d
		hTheta = hTheta + theta(j) .* X(i,j);
		%thetaSqr = thetaSqr + theta(j) .^2;
	end
	gz = sigmoid(hTheta);
	J = J + ((-y(i).*log(gz)) - ((1-y(i)).*log(1-gz)));
	
	grad(1) = grad(1) + ((gz - y(i)).*X(i,1));
	
	for j=2:d
		grad(j) = grad(j) + (((gz - y(i)).*X(i,j)) + ((lambda ./ m) .* theta(j)));
	end
end

%thetaSqr = theta(1) .^2;
for j=2:d
	thetaSqr = thetaSqr + theta(j) .^2;
end

J = (J ./ m) + ((lambda ./ (m .* 2)) .* thetaSqr);
grad = grad ./ m;



% =============================================================

end
