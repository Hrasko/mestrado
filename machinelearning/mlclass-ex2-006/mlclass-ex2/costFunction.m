function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples
d = size(theta);
% You need to return the following variables correctly 
J = 0;
grad = zeros(d);

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%


for i = 1 : m
	hTheta = theta(1);
	for j=2:d
		hTheta = hTheta + theta(j) .* X(i,j);
	end
	gz = sigmoid(hTheta);
	J = J + ((-y(i).*log(gz)) - ((1-y(i)).*log(1-gz)));
	for j=1:d
		grad(j) = grad(j) + ((gz - y(i)).*X(i,j));
	end
end

J = J ./ m;
grad = grad ./ m;



% =============================================================

end
