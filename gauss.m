function [X] = gauss(n,A,b)
    A(:,n+1)=b(:,1);
    for j=1:n-1
       pivot=A(j,j);
       for i=j+1:n 
        A(i,:)=A(i,:)-A(j,:)*A(i,j)/pivot
       end
    end

   X(n)=A(n,n+1)/A(n,n);
   for i=n-1:-1:1
        soma=0;
        for j=i+1:n
           soma=soma+A(i,j)*X(j);
        end
        X(i)=(A(i,n+1)-soma)/A(i,i);
   end
end
